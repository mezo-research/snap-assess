

|                     |      BC|
|:--------------------|-------:|
|TOTAL                | 6997.23|
|Catch                |    0.00|
|Equil_catch          |    0.00|
|Survey               |  661.01|
|Length_comp          | 2871.73|
|Age_comp             | 3405.10|
|Recruitment          |   49.10|
|Forecast_Recruitment |    0.00|
|Parm_priors          |    0.00|
|Parm_softbounds      |    0.00|
|Parm_devs            |    0.00|
|Crash_Pen            |    0.00|
