

|                     |      BC|
|:--------------------|-------:|
|TOTAL                | 9778.01|
|Catch                |    0.00|
|Equil_catch          |    0.00|
|Survey               |  278.96|
|Length_comp          | 2574.43|
|Age_comp             | 6840.58|
|Recruitment          |   77.52|
|Forecast_Recruitment |    0.00|
|Parm_priors          |    0.00|
|Parm_softbounds      |    0.01|
|Parm_devs            |    0.00|
|Crash_Pen            |    0.00|
