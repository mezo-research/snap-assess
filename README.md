## Snapper R Scripts: Automation for Stock Assessment Analyses

These R scripts are designed to improve the repeatability and utility of assessment modelling analyses used for stock assessment of the Western Victoria snapper fishery. 

The scripts described here are written for use with Stock Synthesis 3 (SS3, tested on Version 3.24) and with the R package `r4ss`, version 1.23.5, which can be downloaded from the `r4ss` Github site: https://github.com/r4ss/r4ss. 

Further descriptions and a user guide for each of the current scripts, namely, `ss_out.R`, `ss_lp.R`, and `ss_retro.R`, are provided on the [Wiki](https://github.com/mezo-research/snapper/wiki) page. 

For further information contact [Mezo Research](mezo@mezo.com.au).