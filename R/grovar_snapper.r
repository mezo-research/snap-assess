#######################################################################################
#
# *grovar_snapper.r allows the user to analyse pink snapper
# age-length data from Fisheries Victoria, and to plot growth and summary statistics. 
#
# Plot files are sent to relevant working folders under "Growth"
#
# By: Athol Whitten, Mezo Research
# Contact: athol.whitten@mezo.com.au
# Last Updated: April 2013
########################################################################################

#Remove previous R console object assignments and load required libraries;
rm(list=ls())
library(lattice)
library(Hmisc)

####################################
#Set and load directories/files
####################################

#Set root directory:
rootdir <- "C:/Dropbox"
working.folder <- "C:/Dropbox/Mezo/Projects/Fishvic/Assessment/Snapper"

#Set directory to relevant files:
assess.year <- 2012
data.year <- 2012
user.name <- "Athol Whitten"
data.dir <- paste(working.folder,"Data",sep="/")
data.file <- "/MZ_AgeLen_Snapper.csv"

#Source some functions for plot diagnotics:
source(paste(rootdir,"/Modelling/R/Source/FishFunc.r",sep=""))

#Read data Data (.txt) File:
data.all <- read.csv(paste(data.dir,data.file,sep=""),header=TRUE,comment.char="")

#Read data Data Control (.csv) File;
control <- read.csv(paste(data.dir,"/FV_Data_Control.csv",sep=""),header=TRUE)

#Read in SESSF Parameters (.csv) File;
parms <- read.csv(paste(data.dir,"/FV_Parms.csv",sep=""),header=TRUE)


####################################
#Control Options/Specifications
####################################

#Specify which plots are required: (Specify a number, a series of numbers, "All", or "Data" to get the data into R and check it.)
#which.plots="Data"
which.plots <- "Data"

#Test the length-age residuals for normality/log-normality (TRUE/FALSE)?
test.residuals <- FALSE

#Specify output required (Screen, PDF, or PNG); #PDF not currently available, this needs to be built into the program.
output <- "Screen"

#Bigfonts and lesser years on plots required for presentation style graphics?
present <- FALSE
ifelse(present==TRUE,c(top<-1.5,axs<-1.2,labs<-1.3,tex<-1.15),c(top<-1.0,axs<-1.0,labs<-1.0,tex<-1.0))

#Set some graphical parameters;
windows(record=TRUE) 
if(which.plots=="All"){which.plots <- 1:13}
if(which.plots=="Data"){which.plots <- 1}
opar<-par()

#MFROW set-up (if required, otherwise comment out), which can be used for the compare across species CDG plot.
#par(mfrow=c(3,3))
#Add option above to plot "Comparison Plots", or "Compare" and then choose the shape of the comparison CDG plot based on number of species to compare.

##############################################################################
# Get relevant species data, format data and get parms where required;
##############################################################################

#Get data data for the main species of interest and also for the comparison species if required;
#This is the master loop, everything is repeated from here, over species for which "DoPlots" is TRUE in Control File;

for(i in which(control$DoPlots==TRUE)){
	
	#Get species specific data and assign common name from Control File;
	data.sp=data.all[data.all$Code==control$Code[i],]
	common.name=as.character(control$CommonName[i])
	
	#Get the GEAR to include in, or exclude from the growth analysis (read from Control File);
	gear=as.character(control$Gear[i])
	include.gear=(control$IncludeGear[i])
	if(gear!="All"){
		if(include.gear==TRUE){data<-data[data$Gear==gear,]}
		if(include.gear==FALSE){data<-data[-which(data$Gear==gear),]}
	}

	#Get the ZONE to include in, or exclude from the growth analysis (read from Control File);
	zone=as.character(control$Zone[i])
	include.zone=(control$IncludeZone[i])
	if(zone!="All"){
		if(include.zone==TRUE){data.sp <- data.sp[data.sp$Zone %in% zone,]}
		if(include.zone==FALSE){data.sp <- data.sp[-which(data.sp$Zone==zone),]
		zone=paste("Exc",zone,sep="")
		}
	}
	
	#Get the weight at length parameters if known for males (ma,mb) and females (fa,fb);
	if(control$MFWeight[i]==TRUE){
		m.a=parms$m.a[which(parms$Code==control$Code[i])]
		m.b=parms$m.b[which(parms$Code==control$Code[i])]
		f.a=parms$f.a[which(parms$Code==control$Code[i])]
		f.b=parms$f.b[which(parms$Code==control$Code[i])]
	}
	
	#Set the weight at length parameters as same for all, if not known for males and females separately;
	if(control$MFWeight[i]==FALSE){
		m.a=parms$f.a[which(parms$Code==control$Code[i])]
		m.b=parms$f.b[which(parms$Code==control$Code[i])]
		f.a=parms$f.a[which(parms$Code==control$Code[i])]
		f.b=parms$f.b[which(parms$Code==control$Code[i])]
	}
	
	#NOTE: If there are separate weight at length parameter values available for males and females for a particular species then enter these 
	#in the SESSFParms file. Then ensure that the MFWeight (Male/Female Weight) column is set as TRUE for that species in the Control File. 
	#When this is set to false, the weight at length parameter values given in the SESSFParms file should be put in the female parms columns. 
	#These same parms will be used for both males and females. 
	
#Create new data frame, fix dates, add columns for Cohort, Cohort Name and Sample Year Name:
data.la <- data.all
#data.la$DOC <- as.Date(data.la$DOC,format="%d-%b-%y")
data.la <- cbind(data.la,Year=as.numeric(format(data.la$DOC,"%Y")),Cohort=as.numeric(format(as.Date(data.la$SpawnYr,format="%Y/%y"),"%Y")))
data.la <- cbind(data.la,Cohort.Name=paste("YC",data.la$Cohort),Sample.Year=paste("Year",data.la$Year))
	
#Remove rows where either length or age is zero or there is NA entries:
data.la <- data.la[data.la$Age>0 & data.la$Length>0,]
data.la <- na.omit(data.la)

#Get age of interest from control file (for particular plots);
plot.age=control$PlotAge[i]

#Read in the minimum and maximum cohorts of interest in the control file, they are specific to plotting, not to calculations;
cmin=control$CohortMin[i]
cmax=control$CohortMax[i]
cgd.min=control$CGDMin[i]
cgd.max=control$CGDMax[i]

#If Cohort min/max values are not specified, then get a best guesss;
if(is.na(control$CohortMin[i])){
	cmin=floor(mean(data.la$Cohort)-2*sd(data.la$Cohort))}
if(is.na(control$CohortMax[i])){
	cmax=ceiling(mean(data.la$Cohort)+2*sd(data.la$Cohort))}
	
if(is.na(control$CGDMin[i])){
	cgd.min=floor(mean(data.la$Cohort)-2*sd(data.la$Cohort))}
if(is.na(control$CGDMax[i])){
	cgd.max=ceiling(mean(data.la$Cohort)+2*sd(data.la$Cohort))}	

#Use cmin and cmax to get the number of cohorts for plotting and set plot lengths;	
cnum=length(unique(data.la$Cohort)[unique(data.la$Cohort)>=cmin&unique(data.la$Cohort)<=cmax])
cplotx=round(sqrt(cnum))
cploty=ifelse((sqrt(cnum)-floor(sqrt(cnum)))<=0.5,cplotx+1,cplotx)

#Calculate the plotting lengths and widths in terms of years;
ynum=length(unique(data.la$Year)[unique(data.la$Year)])
yplotx=round(sqrt(ynum))
yploty=ifelse((sqrt(ynum)-floor(sqrt(ynum)))<=0.5,yplotx+1,yplotx)

#Split males and female data sets for species one;
data.la.m=data.la[data.la$Sex==1,]
data.la.f=data.la[data.la$Sex==2,]

#Set some objects to help with plotting (maximum age and length from data set);
amax=max(data.la$Age)
lmax=max(data.la$Length)
amax.plot=max(data.la$Age)+2
lmax.plot=max(data.la$Length)+10

#Get maximum and minimum ages for males and females;
amax.m=max(data.la.m$Age)
lmax.m=max(data.la.m$Length)
amax.f=max(data.la.f$Age)
lmax.f=max(data.la.f$Length)

###################################################
# START ANALYSIS SECTION:
###################################################

#Fit a Von Bertalanffy curve to pooled data, then male and female data. Guess starting values suitable for most species; t0=0, Linf=Guess from LMax, k=0.2 
Li=lmax-10
Li.m=lmax.m-10
Li.f=lmax.f-10
K=0.2
t0=0

#Set VonBert Equation;
VonBert=Length~Li*(1-exp(-K*(Age-t0)))

#Fit and predict for VonBert to all data;
vb.model=nls(VonBert,start=list(Li=Li,K=K,t0=t0),data=data.la)
summary(vb.model)
xv=seq(1,amax,0.1)
yv=predict(vb.model,list(Age=xv))

#Fit and predict for VonBert to Males;
vb.model.m=nls(VonBert,start=list(Li=Li.m,K=K,t0=t0),data=data.la.m)
summary(vb.model.m)
xv.m=seq(1,amax.m,0.1)
yv.m=predict(vb.model.m,list(Age=xv.m))

#Fit and predict for VonBert to Females;
vb.model.f=nls(VonBert,start=list(Li=Li.f,K=K,t0=t0),data=data.la.f)
summary(vb.model.f)
xv.f=seq(1,amax.f,0.1)
yv.f=predict(vb.model.f,list(Age=xv.f))

#Get unique points for benefit of plotting;
data.laa.plot=unique(data.la[,c(8,11)])
data.laa.plot.m=unique(data.la.m[,c(8,11)])
data.laa.plot.f=unique(data.la.f[,c(8,11)])

#Determine weight at age, fit VB across weight for males and females; 
data.wa.m=cbind(data.la.m,Weight=m.a*(data.la.m$Length^m.b))
data.wa.f=cbind(data.la.f,Weight=f.a*(data.la.f$Length^f.b))

wmax.m=max(data.wa.m$Weight)+2
wmax.f=max(data.wa.f$Weight)+2

wamax.m=max(data.wa.m$Age)+3
wamax.f=max(data.wa.f$Age)+3

Wi.m=wmax.m-1
Wi.f=wmax.f-1
K=0.2
t0=0

VonBertW.m=Weight~(Wi.m*(1-exp(-K*(Age-t0))))^m.b
vbw.model.m=nls(VonBertW.m,start=list(Wi.m=Wi.m,K=K,t0=t0),data=data.wa.m)
summary(vbw.model.m)
wxv.m=seq(1,wamax.m,0.1)
wyv.m=predict(vbw.model.m,list(Age=wxv.m))

VonBertW.f=Weight~(Wi.f*(1-exp(-K*(Age-t0))))^f.b
vbw.model.f=nls(VonBertW.f,start=list(Wi.f=Wi.f,K=K,t0=t0),data=data.wa.f)
summary(vbw.model.f)
wxv.f=seq(1,wamax.f,0.1)
wyv.f=predict(vbw.model.f,list(Age=wxv.f))

#Create a new data frame of mean-length-at-age data (by YEAR) and name the columns, include general stats;
data.ymla=aggregate(data.la$Length,list(Year=data.la$Year,Age=data.la$Age),mean)
names(data.ymla)[3]="Mean.Length"
N=(aggregate(data.la$Length,list(Year=data.la$Year,Age=data.la$Age),length))[,3]
Variance=(aggregate(data.la$Length,list(Year=data.la$Year,Age=data.la$Age),var))[,3]
StDev=(aggregate(data.la$Length,list(Year=data.la$Year,Age=data.la$Age),sd))[,3]
SEM=StDev/sqrt(N)

data.ymla=cbind(data.ymla,N,Variance,StDev,SEM)

#Select only mean-length-at-age data where there are more than 4 samples per year, and then sort the data;
data.ymla=data.ymla[N>=4,]
data.ymla=data.ymla[order(data.ymla$Year,data.ymla$Age),]

#Create a new data frame of mean-length-at-age data (by COHORT) and name the columns, include general stats;
data.cmla=aggregate(data.la$Length,list(Cohort=data.la$Cohort,Age=data.la$Age),mean)
names(data.cmla)[3]="Mean.Length"
N=(aggregate(data.la$Length,list(Cohort=data.la$Cohort,Age=data.la$Age),length))[,3]
Variance=(aggregate(data.la$Length,list(Cohort=data.la$Cohort,Age=data.la$Age),var))[,3]
StDev=(aggregate(data.la$Length,list(Cohort=data.la$Cohort,Age=data.la$Age),sd))[,3]
SEM=StDev/sqrt(N)
MLA=aggregate(data.la$Length,list(Age=data.la$Age),mean)
names(MLA)=c("Age","Mean.Length")
Mean.Exp.LAA=MLA$Mean.Length[match(data.cmla$Age,MLA$Age)]
MLA.Resid=data.cmla[,"Mean.Length"]/Mean.Exp.LAA

#CHECK THIS: What is the MLA.Resid used for? Seems it is not actually used any further in this code. 

#Create the data frame and add a column for cohort name;
data.cmla=cbind(data.cmla,N,Variance,StDev,SEM,MLA.Resid,Cohort.Name=paste("YC",data.cmla$Cohort))

#Create a mean length-at-age dataframe that accounts for cohort sampling effects (by resampling);
data.SBC=data.frame()

	for (cohort in unique(data.la$Cohort)){
			
		next.set <- data.la[sample(which(data.la$Cohort==cohort),1000,replace=TRUE),]
		data.SBC=rbind(data.SBC,next.set)
		
	}
	
	head(data.SBC)

	cohortcount=aggregate(data.la$Cohort,list(Cohort=data.la$Cohort),length)
    names(cohortcount)[2]="Samples"
    cohortcount=cohortcount[rev(order(cohortcount$Samples)),]
    row.names(cohortcount)=NULL
    
	
	SBC.cohortcount=aggregate(data.SBC$Cohort,list(Cohort=data.SBC$Cohort),length)
    names(SBC.cohortcount)[2]="Samples"
    SBC.cohortcount=SBC.cohortcount[rev(order(SBC.cohortcount$Samples)),]
    row.names(SBC.cohortcount)=NULL
    
	
#CHECK THIS: Why do more samples appear for most numerous cohorts!? Is this a result of the sampling method? This could be used to resample for snapper recruitment as well?

SBC.MLA=aggregate(data.SBC$Length,list(Age=data.SBC$Age),mean)
names(SBC.MLA)=c("Age","Mean.Length")
	
#Create a mean length-at-age dataframe that accounts for year-by-year sampling effects (by resampling);
data.SBY=data.frame()

for (year in unique(data.la$Year)){
			
		next.set <- data.la[sample(which(data.la$Year==year),500,replace=TRUE),]
		data.SBY=rbind(data.SBY,next.set)
		
	}
	
	head(data.SBY)

	SBY.cohortcount=aggregate(data.SBY$Cohort,list(Cohort=data.SBY$Cohort),length)
    names(SBY.cohortcount)[2]="Samples"
    SBY.cohortcount=SBY.cohortcount[rev(order(SBY.cohortcount$Samples)),]
    row.names(SBY.cohortcount)=NULL
    
	
	SBY.yearcount=aggregate(data.SBY$Year,list(Year=data.SBY$Year),length)
    names(SBY.yearcount)[2]="Samples"
    SBY.yearcount=SBY.yearcount[rev(order(SBY.yearcount$Samples)),]
    row.names(SBY.yearcount)=NULL
    

SBY.MLA=aggregate(data.SBY$Length,list(Age=data.SBY$Age),mean)
names(SBY.MLA)=c("Age","Mean.Length")
	
#Select only mean-length-at-age data where there are more than 4 samples per cohort, and then sort the data;
data.cmla=data.cmla[N>=4,]
data.cmla=data.cmla[order(data.cmla$Cohort,data.cmla$Age),]

#Create data frame for summary statistic of difference between observed and predicted LENGTH at age data points (by COHORT);
data.cgd=data.la[data.la$Age>0&data.la$Length>0,]
data.cgd=data.cgd[data.cgd$Cohort>=cgd.min&data.cgd$Cohort<=cgd.max,]

#Get difference between expected and observed lengths and create data frame (determine Cohort Growth Deviations);
exp.length=predict(vb.model,list(Age=data.cgd$Age))
data.cgd=cbind(data.cgd,Exp.Length=exp.length)
exp.obs=(data.cgd$Length-data.cgd$Exp.Length)/data.cgd$Exp.Length
data.cgd=cbind(data.cgd,Grow.Dev=exp.obs)
Cht.Grow.Dev=stack(by(data.cgd$Grow.Dev,data.cgd$Cohort,mean))
Cht.Grow.SD=stack(by(data.cgd$Grow.Dev,data.cgd$Cohort,sd))
gdevlim=c(min(Cht.Grow.Dev$values)*1.20,max((Cht.Grow.Dev$values)*1.20))
Cht.Grow.Dev.Mean=mean(Cht.Grow.Dev$values)
Cht.Grow.Dev.SD=sd(Cht.Grow.Dev$values)

#Determine the Cohort Growth Deviation in terms of WEIGHT, CDG.W (for females);
data.wa.f=data.wa.f[data.wa.f$Cohort>=cgd.min&data.wa.f$Cohort<=cgd.max,]
exp.weight.f=predict(vbw.model.f,list(Age=data.wa.f$Age))
data.wa.f=cbind(data.wa.f,Exp.Weight=exp.weight.f)
exp.obs=(data.wa.f$Weight-data.wa.f$Exp.Weight)/data.wa.f$Exp.Weight
data.wa.f=cbind(data.wa.f,Weight.Dev=exp.obs)
Cht.Weight.Dev=stack(by(data.wa.f$Weight.Dev,data.wa.f$Cohort,mean))
gdevlim.w=c(min(Cht.Weight.Dev$values)*1.05,max((Cht.Weight.Dev$values)*1.05))
Cht.Weight.Dev.Mean=mean(Cht.Weight.Dev$values)

#Determine the Yearly Growth Deviation Statistic;
data.ygd=data.la[data.la$Age>0&data.la$Length>0,]
exp.length=predict(vb.model,list(Age=data.ygd$Age))
data.ygd=cbind(data.ygd,Exp.Length=exp.length)
exp.obs=(data.ygd$Length-data.ygd$Exp.Length)/data.ygd$Exp.Length
data.ygd=cbind(data.ygd,Yr.Dev=exp.obs)
Yr.Grow.Dev=stack(by(data.ygd$Yr.Dev,data.ygd$Year,mean))
Yr.Grow.SD=stack(by(data.ygd$Yr.Dev,data.ygd$Year,sd))
ydevlim=c(min(Yr.Grow.Dev$values)*1.20,max((Yr.Grow.Dev$values)*1.20))
Yr.Grow.Dev.Mean=mean(Yr.Grow.Dev$values)
Yr.Grow.Dev.SD=sd(Yr.Grow.Dev$values)

#Determine the Yearly Growth Deviation in terms of WEIGHT, CDG.W (for females);
Yr.Weight.Dev=stack(by(data.wa.f$Weight.Dev,data.wa.f$Year,mean))
ydevlim.w=c(min(Yr.Weight.Dev$values)*1.20,max((Yr.Weight.Dev$values)*1.20))
Yr.Weight.Dev.Mean=mean(Cht.Weight.Dev$values)

	
	#If a comparison species is specified, then many of the data preparation lines and analysis sections are repeated for species two:
	if(control$Compare[i]==TRUE){
		
		#Set the row number for the species with which to compare;
		j=which(control$NameCode==as.character(control$CompareWith[i]))
	
		#Get the data  and common name for compare species;
		data.2=data.all[data.all$Code==control$Code[j],]
		common.name.2=as.character(control$CommonName[j])
		
		#Add a column for Cohort and a column for Cohort Name and Sample Year Name;
		data.2=cbind(data.2,Cohort=data.2$Year-data.2$Age)
		data.2=cbind(data.2,Cohort.Name=paste("YC",data.2$Cohort),Sample.Year=paste("Year ",data.2$Year))
			
		#Create a subset dataframe with columns of interest (in a new order) for length/age analysis and remove zeros;
		data.la.2=data.2[data.2$Age>0&data.2$Length>0,c(1,2,6:9,11,17,18,14)] 
		#Review this change (removal of data), it might be better to keep all data in place.
		
		#Set some objects to help with fitting and plotting (maximum age and length from data set);	
		amax.2=max(data.la.2$Age)
		lmax.2=max(data.la.2$Length)
		
		amax.2.plot=max(data.la.2$Age)+2
		lmax.2.plot=max(data.la.2$Length)+10
				
		amax.both.plot=max(c(amax,amax.2))+2
		lmax.both.plot=max(c(lmax,lmax.2))+10
		
		#Fit and predict for VonBert to Species Two;
		vb.model.2=nls(VonBert,start=list(Li=Li,K=K,t0=t0),data=data.la.2)
		summary(vb.model.2)
		xv.2=seq(1,amax.2,0.1)
		yv.2=predict(vb.model.2,list(Age=xv.2))
	
		#Create a new data frame of mean-length-at-age data (by COHORT) and name the columns, include general stats;
		data.cmla.2=aggregate(data.la.2$Length,list(Cohort=data.la.2$Cohort,Age=data.la.2$Age),mean)
		names(data.cmla.2)[3]="Mean.Length"
		N.2=(aggregate(data.la.2$Length,list(Cohort=data.la.2$Cohort,Age=data.la.2$Age),length))[,3]
		Variance.2=(aggregate(data.la.2$Length,list(Cohort=data.la.2$Cohort,Age=data.la.2$Age),var))[,3]
		StDev.2=(aggregate(data.la.2$Length,list(Cohort=data.la.2$Cohort,Age=data.la.2$Age),sd))[,3]
		SEM.2=StDev.2/sqrt(N.2)

		data.cmla.2=cbind(data.cmla.2,N.2,Variance.2,StDev.2,SEM.2)

		#Select only mean-length-at-age data where there are more than 4 samples per cohort, and then sort the data;
		data.cmla.2=data.cmla.2[N.2>=4,]
		data.cmla.2=data.cmla.2[order(data.cmla.2$Cohort,data.cmla.2$Age),]
	
		#Create data frame for summary statistic of difference between observed and predicted LENGTH at age data points (by COHORT);
		data.cgd.2=data.2[data.2$Age>0&data.2$Length>0,c(2,6:9,11,17,18,14)]
		data.cgd.2=data.cgd.2[data.cgd.2$Cohort>=cgd.min&data.cgd.2$Cohort<=cgd.max,]

		#Get difference between expected and observed lengths and create data frame;
		exp.length.2=predict(vb.model.2,list(Age=data.cgd.2$Age))
		data.cgd.2=cbind(data.cgd.2,Exp.Length=exp.length.2)
		names(data.cgd.2)[length(data.cgd.2)]="Exp.Length"
		exp.obs.2=(data.cgd.2$Length-data.cgd.2$Exp.Length)/data.cgd.2$Exp.Length
		data.cgd.2=cbind(data.cgd.2,Grow.Dev=exp.obs.2)
		Cht.Grow.Dev.2=stack(by(data.cgd.2$Grow.Dev,data.cgd.2$Cohort,mean))
		gdevlim.2=c(min(Cht.Grow.Dev.2$values)*1.20,max((Cht.Grow.Dev.2$values)*1.20))
		Cht.Grow.Dev.Mean.2=mean(Cht.Grow.Dev.2$values)
	
	}

################################################################################
# START PLOTS SECTION:
################################################################################

#Set folder to output plots;
which.plots <- c(1,2,3)  
out.dir=paste(working.folder,"Assessment",common.name,"Growth",sep="/")
if(present==TRUE){out.dir="C:/Desktop"}
dir.create(out.dir)

# PLOT SET 1: Basic Growth Analysis. Plot observed and expected growth, 
# and compare growth curves between males and females and also with comparison species if required.
if(1 %in% which.plots){

	#Plot all growth pooled;
	plot(data.laa.plot$Age,data.laa.plot$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",main=paste("Observed & Predicted Length at Age for ",common.name,sep=""))
	lines(xv,yv,col="black",lty=1,lwd=3)

	if(output=="PNG"){
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","Expected_LAA.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")

		plot(data.laa.plot$Age,data.laa.plot$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)")
		lines(xv,yv,col="black",lty=1,lwd=3)

		dev.off()
	}
	
	#Plot male and female growth separately;
	plot(data.laa.plot.m$Age,data.laa.plot.m$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",main=paste("Observed Length at Age for ",common.name,sep=""),abline(h = 0, col = "grey"))
	grid()
	points(data.laa.plot.f$Age,data.laa.plot.f$Length,col="grey")
	lines(xv.f,yv.f,col="Red",lty=1,lwd=3)
	lines(xv.m,yv.m,col="Dark Blue",lty=1,lwd=3)
	legend("topleft", bty = "n", c("Females", "Males"),lty = 1,lwd=2, col = c("Red", "Dark Blue"))
	
	#Send plot to PNG;
	if(output=="PNG"){
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","MF_Expected_LAA.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
		
		plot(data.laa.plot.m$Age,data.laa.plot.m$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",abline(h = 0, col = "grey"))
		grid()
		points(data.laa.plot.f$Age,data.laa.plot.f$Length,col="Grey")
		lines(xv.f,yv.f,col="Red",lty=1,lwd=3)
		lines(xv.m,yv.m,col="Dark Blue",lty=1,lwd=3)
		legend("topleft", bty = "n", c("Females", "Males"),lty = 1,lwd=2, col = c("Red", "Dark Blue"))
		dev.off()
		}
	
	#Compare mean length-at-age data with VonBert model on a plot;
	plot(data.laa.plot$Age,data.laa.plot$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",main=paste("Predicted vs. Observed Mean Length-at-age for ",common.name,sep=""))
	lines(xv,yv,col="black",lty=1,lwd=2)
	MLA=aggregate(data.la$Length,list(Age=data.la$Age),mean)
	names(MLA)=c("Age","Mean.Length")
	points(MLA,col="Red",pch=18,type="l",lwd=2)
	

	#Send plot to PNG;	
	if(output=="PNG"){
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","Obs_Vs_Exp_LAA.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
		plot(data.laa.plot$Age,data.laa.plot$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)")
		lines(xv,yv,col="black",lty=1,lwd=2)
		MLA=aggregate(data.la$Length,list(Age=data.la$Age),mean)
		names(MLA)=c("Age","Mean.Length")
		points(MLA,col="Red",pch=18,type="l",lwd=2)
		
		dev.off()
	}
		
	#Plot Comparison Species Growth if required;
	if(control$Compare[i]==TRUE){
	
		#Create a simple set of data to speed plotting (suitable for plotting only);
		data.laa.plot.2=unique(data.la.2[,c(6,7)])

		#Plot growth for species two;
		plot(data.laa.plot.2$Age,data.laa.plot.2$Length,pch=1,col="grey",xlim=c(0,amax.2.plot),ylim=c(0,lmax.2.plot),xlab="Age (yr)",ylab="Fork Length (cm)",main=paste("Observed & Predicted Length at Age for ",common.name.2,sep=""))
		lines(xv.2,yv.2,col="black",lty=1,lwd=3)

		if(output=="PNG"){
			png(paste(out.dir,"/",common.name.2,"_",gear,"_",zone,"_","Expected_LAA.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
			plot(data.laa.plot.2$Age,data.laa.plot.2$Length,pch=1,col="grey",xlim=c(0,amax.2),ylim=c(0,lmax.2),xlab="Age (yr)",ylab="Fork Length (cm)")
			lines(xv.2,yv.2,col="black",lty=1,lwd=3)
			
			dev.off()
		}
	
		#Plot species one c.f. species two;
		plot(data.laa.plot$Age,data.laa.plot$Length,pch=1,col="Light Blue",xlim=c(0,amax.both.plot),ylim=c(0,lmax.both.plot),xlab="Age (yr)",ylab="Fork Length (cm)",main=paste("Length at Age for ",common.name," cf. ",common.name.2,sep=""))
		points(data.laa.plot.2$Age,data.laa.plot.2$Length,pch=1,col="Pink")
		lines(xv,yv,col="Blue",lty=1,lwd=3)
		lines(xv.2,yv.2,col="Red",lty=1,lwd=3)
		legend("topleft",bty ="n",c(common.name,common.name.2),lty=1,lwd=2,col=c("Blue","Red"))

		if(output=="PNG"){
		
			png(paste(out.dir,"/",common.name,"_",common.name.2,"_",gear,"_",zone,"_","Expected_LAA.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
			plot(data.laa.plot$Age,data.laa.plot$Length,pch=1,col="Light Blue",xlim=c(0,amax.both.plot),ylim=c(0,lmax.both.plot),xlab="Age (yr)",ylab="Fork Length (cm)")
			points(data.laa.plot.2$Age,data.laa.plot.2$Length,pch=1,col="Pink")
			lines(xv,yv,col="Blue",lty=1,lwd=3)
			lines(xv.2,yv.2,col="Red",lty=1,lwd=3)
			legend("topleft",bty ="n",c(common.name,common.name.2),lty=1,lwd=2,col=c("Blue","Red"))
			
			dev.off()
		}
	
	}
}


# PLOT 2: Weight-at-age plot. (Calculated weight at age from weight-at-length relationship).
if(2 %in% which.plots){

		#Plot the calculated weight-at-age points and relationship;
		plot(data.wa.m$Age,data.wa.m$Weight,pch=1,col="grey",xlim=c(0,wamax.f),ylim=c(0,wmax.f),xlab="Age (yr)",ylab="Calculated Weight (kg)",main=paste("Expected Weight at Age for ",common.name," (calculated)",sep=""))
		grid()
		points(data.wa.f$Age,data.wa.f$Weight,col="Grey")
		lines(wxv.f,wyv.f,col="Red",lty=1,lwd=2)
		lines(wxv.m,wyv.m,col="Dark Blue",lty=1,lwd=2)
		legend("topleft", bty = "n", c("Females", "Males"),lty = 1,lwd=2, col = c("Red", "Dark Blue"))

		
		if(output=="PNG"){
			png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","Expected_WAA.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")

			plot(data.wa.m$Age,data.wa.m$Weight,pch=1,col="grey",xlim=c(0,wamax.f),ylim=c(0,wmax.f),xlab="Age (yr)",ylab="Calculated Weight (kg)")
			grid()
			points(data.wa.f$Age,data.wa.f$Weight,col="Grey")
			lines(wxv.f,wyv.f,col="Red",lty=1,lwd=2)
			lines(wxv.m,wyv.m,col="Dark Blue",lty=1,lwd=2)
			legend("topleft", bty = "n", c("Females", "Males"),lty = 1,lwd=2, col = c("Red", "Dark Blue"))
  
			dev.off()
		}
	}

	
# PLOT SET 3: Plots of length-at-age across years and/or cohorts, analysis of deviations from mean (or expected) growth; 
if(3 %in% which.plots){

	#PLOT 3A - 'Length-at-age' residuals, 
	#Get data for cohorts of interest and create 'length-at-age' residuals;
	vg.data.la=data.la[data.la$Cohort>=cmin&data.la$Cohort<=cmax,]
	Exp.LAA=MLA$Mean.Length[match(vg.data.la$Age,MLA$Age)]
	LA.Resid=vg.data.la[,"Length"]/Exp.LAA 	#Note, the question here remains, which to use? The additive or multiplicative residuals? Change this according to inference based on the normality tests below.
	Log.LA.Resid=log(LA.Resid)
	vg.data.la=cbind(vg.data.la,LA.Resid=LA.Resid,Log.LA.Resid=Log.LA.Resid)
	
	#Plot box plot version of 'length-at-age' residuals;
	la.bp=boxplot(LA.Resid ~ Cohort, data=vg.data.la,main=paste("Length-at-age residuals for ",common.name,sep=""),ylab="Length-at-age residuals",xlab="Cohort")
	abline(h=1,lty=5,col="black")
	
	#Send plot to PNG if required;
		if(output=="PNG"){
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","LA_Residuals_Boxplot.png",sep=""),width=25,height=20,res=300,units="cm",pointsize=14,bg="White")
		par(las=2, mar=c(5, 4, 4, 2) + 0.1)
		boxplot(LA.Resid ~ Cohort, data=vg.data.la,ylab="Length-at-age residuals",ylim=c(0.4,2))
		abline(h=1,lty=5,col="black")
		text(1:(length(as.numeric(la.bp$names))),rep(1.9,length(as.numeric(la.bp$names))),la.bp$n,srt=90,cex=0.75)
		par(opar)
		mtext("Cohort", side=1, line=3.5)
		dev.off()
		}
		
	
	#At this point: We could also group 'increment-at-age' residuals by cohort, and look at cohort-specific increment effects. 
	
	#PLOT 3B - Yearly 'increment-at-age' residuals;
	#Create an increment-residuals plot of growth at age increments by year;
	#Calculate increment from age in previous year (following cohorts);

	inc.cohort=data.frame()    
	  
	  for(k in unique(data.cmla$Cohort)){
	  
	  cohort.data=data.cmla[data.cmla$Cohort==k,]

	  for(j in (cohort.data$Age)){ 
							
			 size.b=cohort.data[cohort.data$Age==j,"Mean.Length"]
			 size.a=cohort.data[cohort.data$Age==(j-1),"Mean.Length"]
			 increment=size.b-size.a         
			 if(j==1){increment=size.b}
			 if(sum(increment)==0){increment=0}
			 
			 next.row=cbind(Cohort=k,Age=j,Increment=increment)
			 inc.cohort=rbind(inc.cohort,next.row)
			 } 
		   } 
		
	inc.cohort=inc.cohort[inc.cohort$Increment>0,]
	#inc.cohort=inc.cohort[inc.cohort$Age<16,]
	inc.cohort=cbind(Year=inc.cohort$Cohort+inc.cohort$Age,inc.cohort)
	   
	#Aggegate the data and get mean increments for each age (for the expected increment-by-age) these should be similar to the differences between the mean length-at-age;
	mean.increments=aggregate(inc.cohort$Increment,list(Age=inc.cohort$Age),mean)
	names(mean.increments)=c("Age","Mean.Inc")
	Mean.Exp.Inc=mean.increments$Mean.Inc[match(inc.cohort$Age,mean.increments$Age)]

	#Calculate the 'increment residuals' and add them to the data.frame;
	Inc.Resid=inc.cohort[,"Increment"]/Mean.Exp.Inc
	
	inc.cohort=cbind(inc.cohort,Inc.Resid=Inc.Resid,Log.Inc.Resid=log(Inc.Resid))
	
	
	#Alternative calculation of 'increment residuals', this time where the expected increments are calculated from the from the SBY.MLA data frame;
	SBY.MLA.Inc=SBY.MLA-rbind(c(0,0),SBY.MLA[-(nrow(SBY.MLA)),])
	
	#Alternative no. two: from VB equation for SBY data.
	SBY.vb.model=nls(VonBert,start=list(Li=Li,K=K,t0=t0),data=data.SBY)
	summary(SBY.vb.model)
	SBY.xv=seq(1,amax)
	SBY.yv=predict(vb.model,list(Age=SBY.xv))
	SBY.VB=cbind("Age"=SBY.xv,"Length"=predict(vb.model,list(Age=SBY.xv)))
	
	SBY.VB.Inc=as.data.frame(cbind("Age"=SBY.xv,"Exp.Inc"=SBY.yv-append(SBY.yv[-length(SBY.yv)],0,0)))
	
	SBY.Exp.Inc=SBY.VB.Inc$Exp.Inc[match(inc.cohort$Age,SBY.VB.Inc$Age)]
	
	#Calculate the new 'increment residuals' and replace the old ones in the dataframe;
	
	SBY.Inc.Resid=inc.cohort[,"Increment"]/SBY.Exp.Inc
	
	inc.cohort=cbind(inc.cohort,SBY.Inc.Resid=SBY.Inc.Resid,Log.Inc.Resid=log(SBY.Inc.Resid))

		
		#Plot boxplot version of yearly growth increment residuals;
		par(las=2)
		inc.bp=boxplot(Inc.Resid ~ Year, data=inc.cohort, main=paste("Increment residuals for ",common.name,sep=""),ylab="Estimated growth increment residuals")
		abline(h=1,lty=5,col="black")
		par(opar)
		
		
		#Send boxplot to PNG if required;
			if(output=="PNG"){
			png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","Increment_Residuals_Boxplot.png",sep=""),width=25,height=20,res=300,units="cm",pointsize=14,bg="White")
			par(las=2,mar= c(5, 4, 4, 2) + 0.1)
			boxplot(Inc.Resid ~ Year, data=inc.cohort, ylab="Estimated growth increment residuals",ylim=c(-1,5))
			abline(h=1,lty=5,col="black")
			text(1:(length(as.numeric(inc.bp$names))),rep(4.5,length(as.numeric(inc.bp$names))),inc.bp$n,srt=90,cex=0.75)
			par(opar)
			mtext("Year", side=1, line=3.5)
			dev.off()
			}
			
			
	#PLOT 3C - 'Bubble plot'; (NOTE: Bubble plot has been created specifically for Blue Grenadier and is configured as such).
	
	#Get all data up to a specified maximum age (Here 7);
	data.ymla.bubble=data.ymla[data.ymla$Age<=7,]
	
	#Get all data up to a specified maximum year (Here 2007);
	data.ymla.bubble=data.ymla.bubble[data.ymla.bubble$Year<=2007,]
	
	#Add a column for cohort;
	data.ymla.bubble=cbind(Cohort=data.ymla.bubble$Year-data.ymla.bubble$Age,data.ymla.bubble)
	
	#Assign bubble plot x,y,z values;
	x=data.ymla.bubble$Year
	y=data.ymla.bubble$Age 
	z=data.ymla.bubble$Mean.Length 
	
	#Plot mean length-at-age across years;
	symbols(x,y,circles=z,inches=0.18,fg="Black",bg="Light Grey",xlab="Year",ylab="Age")
	
	#Mark particular cohorts for Blue Grenadier;
		if(common.name=="Blue Grenadier"){
			cohort1=1994
			cohort2=1992
			cohort3=2004
			
			points(x=data.ymla.bubble$Year[data.ymla.bubble$Cohort==cohort1],y=data.ymla.bubble$Age[data.ymla.bubble$Cohort==cohort1],col="black",pch=20,cex=1.5)
			points(x=data.ymla.bubble$Year[data.ymla.bubble$Cohort==cohort2],y=data.ymla.bubble$Age[data.ymla.bubble$Cohort==cohort2],col="black",pch=21,lwd=2,cex=1.2)
			points(x=data.ymla.bubble$Year[data.ymla.bubble$Cohort==cohort3],y=data.ymla.bubble$Age[data.ymla.bubble$Cohort==cohort3],col="black",pch=21,lwd=2,cex=1.2)
		}
		
	if(output=="PNG"){
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","MLA_Bubble_Plot.png",sep=""),width=25,height=20,res=300,units="cm",pointsize=14,bg="White")
		symbols(x,y,circles=z,inches=0.20,fg="Black",bg="Light Grey",xlab="Year",ylab="Age")
		
		
		#Mark particular cohorts for Blue Grenadier;
		if(common.name=="Blue Grenadier"){
			cohort1=1994
			cohort2=1992
			cohort3=2004
			
			points(x=data.ymla.bubble$Year[data.ymla.bubble$Cohort==cohort1],y=data.ymla.bubble$Age[data.ymla.bubble$Cohort==cohort1],col="black",pch=20,cex=1.5)
			points(x=data.ymla.bubble$Year[data.ymla.bubble$Cohort==cohort2],y=data.ymla.bubble$Age[data.ymla.bubble$Cohort==cohort2],col="black",pch=21,lwd=2,cex=1.2)
			points(x=data.ymla.bubble$Year[data.ymla.bubble$Cohort==cohort3],y=data.ymla.bubble$Age[data.ymla.bubble$Cohort==cohort3],col="black",pch=21,lwd=2,cex=1.2)
		}
		
		dev.off()
	}
}	


# PLOT 4: Lattice plot of cohort specific observed length-at-age data, with overlay of VB fit to all data;
if(4 %in% which.plots){

	#Create data frame for a lattice plot of length-at-age data;
	data.cla=data.la[data.la$Cohort>=cmin&data.la$Cohort<=cmax,]
	data.cla.plot=unique(data.cla)
	
	#Create plotting function using xyplot (requires lattice package), remember to frame xyplot with print() to complete before closing graphics device.
	cla.plot<-function(main){print(xyplot(Length~Age|Cohort.Name,xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age(Yr)",ylab="Mean Length(cm)",between=list(x=0.3,y=0.3),main=main,
    layout=c(cplotx,cploty),data=data.cla.plot,as.table=T,
		panel=function(x,y){
		panel.xyplot(x,y,pch=23,type="p",col="grey")
		panel.lines(xv,yv,col="black",lty=2)
	}))}
	
	#Print plot to screen;
	cla.plot(main=paste("Observed Length at Age by Cohort for ",common.name,sep=""))
	
	#Print plot to PNG;
	if(output=="PNG"){
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","LAA_by_Cohort.png",sep=""),width=25,height=20,res=300,units="cm",pointsize=14,bg="White")
		cla.plot(main=NULL)
		dev.off()
	}
}


# PLOT 5: Lattice plot of year specific observed length-at-age data, with overlay of VB fit to all data;
if(5 %in% which.plots){

	#Create data frame for a lattice plot of length-at-age data;
	data.yla.plot=cbind(data.la,Sample.Year=paste("Year",data.la$Year))
	data.yla.plot=unique(data.yla.plot)
	
	#Create plotting function using xyplot;
	yla.plot<-function(main){print(xyplot(Length~Age|Sample.Year,xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age(Yr)",ylab="Mean Length(cm)",between=list(x=0.3,y=0.3),main=main,
		layout=c(yplotx,yploty),data=data.yla.plot,as.table=T,
		panel=function(x,y){
		panel.xyplot(x,y,pch=23,type="p",col="grey")
		panel.lines(xv,yv,col="black",lty=2)
	}))}
	
	#Print plot to screen;
	yla.plot(main=paste("Observed Length at Age by Year for ",common.name,sep=""))
	
	#Print plot to PNG; 
	if(output=="PNG"){	
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","LAA_by_Year.png",sep=""),width=25,height=20,res=300,units="cm",pointsize=14,bg="White")
		yla.plot(main=NULL)
		dev.off()
	}
}


# PLOT 6: Lattice plot of cohort specific mean-length-at-age data, with overlay of VB fit (and with error bars);
if(6 %in% which.plots){
	
	#Create data frame for a lattice plot of mean-length-at-age data;
	data.cmla.plot=data.cmla[data.cmla$Cohort>=cmin&data.cmla$Cohort<=cmax,]
	
	#Create plotting function using xYplot (requires Hmisc package);
	cmla.plot<-function(main){print(xYplot(Cbind(Mean.Length,Mean.Length-1.96*SEM,Mean.Length+1.96*SEM)~Age|Cohort.Name,xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age(Yr)",ylab="Mean Length(cm)",between=list(x=0.3,y=0.3),main=main,
	   layout=c(cplotx,cploty),data=data.cmla.plot,as.table=T,
	   panel=function(x,y){
	   panel.xYplot(x,y,pch=18,type="o",col="blue")
	   panel.lines(xv,yv,col="black",lty=2)
	}))}

   #Print plot to screen;
   cmla.plot(main=paste("Mean Observed Length at Age by Cohort for ",common.name,sep=""))
    
   #Print plot to PNG;
   if(output=="PNG"){
	   png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","MLAA_by_Cohort.png",sep=""),width=25,height=20,res=300,units="cm",pointsize=14,bg="White")
	   cmla.plot(main=NULL)
	   dev.off()
	}
}


# PLOT 7: Lattice plot of year specific mean-length-at-age data, with overlay of VB fit (and with error bars);
if(7 %in% which.plots){
	
	#Create data frame for a lattice plot of length-at-age data;
	data.ymla.plot=cbind(data.ymla,Sample.Year=paste("Year",data.ymla$Year))
	
	#Create plotting function using xYplot;
	ymla.plot<-function(main){print(xYplot(Cbind(Mean.Length,Mean.Length-1.96*SEM,Mean.Length+1.96*SEM)~Age|Sample.Year,xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age(Yr)",ylab="Mean Length(cm)",between=list(x=0.3,y=0.3),main=main,
	   layout=c(yplotx,yploty),data=data.ymla.plot,as.table=T,
	   panel=function(x,y){
	   panel.xYplot(x,y,pch=18,type="p",col="blue")
	   panel.lines(xv,yv,col="black",lty=2)
	   }))}
	
	#Print plot to screen;
	ymla.plot(main=paste("Mean Observed Length at Age by Year for ",common.name,sep=""))
		
	#Print plot to PNG;
	if(output=="PNG"){
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","MLAA_by_Year.png",sep=""),width=25,height=20,res=300,units="cm",pointsize=14,bg="White")
		ymla.plot(main=NULL)
		dev.off()
	}
}


# PLOT SET 8: Time series plots of mean size at age (by COHORT, and YEAR) for age of interest (then compare with expected);
if(8 %in% which.plots){

	#Create time-series data frames for plotting;
	data.cmla.ts=cbind(data.cmla,Years.Old=paste(data.cmla$Age," Year Olds",sep=""))
	data.ymla.ts=cbind(data.ymla,Years.Old=paste(data.ymla$Age," Year Olds",sep=""))
	
	#Get max and min lengths for plotting;
	lmin.ts=min(data.cmla.ts[data.cmla.ts$Age==plot.age,"Mean.Length"])-10
	lmax.ts=max(data.cmla.ts[data.cmla.ts$Age==plot.age,"Mean.Length"])+10

	#Plot the mean length-at-age of interest by cohort;
	plot(data.cmla.ts[data.cmla.ts$Age==plot.age,"Mean.Length"]~data.cmla.ts[data.cmla.ts$Age==plot.age,"Cohort"],main=paste("Length at Age ",plot.age," by Cohort",sep=""),pch=18,type="b",lwd=2,ylim=c(lmin.ts,lmax.ts),xlab="Cohort",ylab="Length (cm) at Age",col="Blue")
	error.bar(data.cmla.ts[data.cmla.ts$Age==plot.age,"Cohort"],data.cmla.ts[data.cmla.ts$Age==plot.age,"Mean.Length"],data.cmla.ts[data.cmla.ts$Age==plot.age,"SEM"]*1.96)
	latage.age=predict(vb.model,list(Age=plot.age))
	abline(h=latage.age,lty=3)
	
	#Plot the mean length-at-age of interest by year;
	plot(data.ymla.ts[data.ymla.ts$Age==plot.age,"Mean.Length"]~data.ymla.ts[data.ymla.ts$Age==plot.age,"Year"],main=paste("Length at Age ",plot.age," by Sample Year",sep=""),pch=18,type="b",lwd=2,ylim=c(lmin.ts,lmax.ts),xlab="Year",ylab="Length (cm) at Age",col="Blue")
	error.bar(data.ymla.ts[data.ymla.ts$Age==plot.age,"Year"],data.ymla.ts[data.ymla.ts$Age==plot.age,"Mean.Length"],data.ymla.ts[data.ymla.ts$Age==plot.age,"SEM"]*1.96)
	abline(h=latage.age,lty=3)

	#Send plots to PNG;
	if(output=="PNG"){
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","LAge",plot.age,"_by_Cohort.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="White")
		plot(data.cmla.ts[data.cmla.ts$Age==plot.age,"Mean.Length"]~data.cmla.ts[data.cmla.ts$Age==plot.age,"Cohort"],pch=18,type="b",lwd=2,ylim=c(lmin.ts,lmax.ts),xlab="Cohort",ylab="Length (cm) at Age",col="Blue")
		error.bar(data.cmla.ts[data.cmla.ts$Age==plot.age,"Cohort"],data.cmla.ts[data.cmla.ts$Age==plot.age,"Mean.Length"],data.cmla.ts[data.cmla.ts$Age==plot.age,"SEM"]*1.96)
		latage.age=predict(vb.model,list(Age=plot.age))
		abline(h=latage.age,lty=3)
		dev.off()
	}
	
	if(output=="PNG"){
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","LAge",plot.age,"_by_Year.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="White")
		plot(data.ymla.ts[data.ymla.ts$Age==plot.age,"Mean.Length"]~data.ymla.ts[data.ymla.ts$Age==plot.age,"Year"],pch=18,type="b",lwd=2,ylim=c(lmin.ts,lmax.ts),xlab="Year",ylab="Length (cm) at Age",col="Blue")
		error.bar(data.ymla.ts[data.ymla.ts$Age==plot.age,"Year"],data.ymla.ts[data.ymla.ts$Age==plot.age,"Mean.Length"],data.ymla.ts[data.ymla.ts$Age==plot.age,"SEM"]*1.96)
		latage.age=predict(vb.model,list(Age=plot.age))
		abline(h=latage.age,lty=3)
		dev.off()
	}

}
	
#PLOT SET 9: CGD statistic for species one and compared with species two if required;
if(9 %in% which.plots){

	#Plot the CGD statistic for species one;
	plot(as.numeric(as.character(Cht.Grow.Dev$ind)),Cht.Grow.Dev$values,ylab="Cohort Growth Deviation",xlab="Cohort",type="b",lwd=2,ylim=gdevlim,mfrow=c(2,1),col="Blue",xlim=c(cgd.min-1,cgd.max+3),main=paste("Cohort Growth Deviation for ",common.name,sep="")) 
	abline(h=0,lty=3,lwd=1,col="grey")
	arrows(cgd.max+1.75,max(Cht.Grow.Dev$values),cgd.max+1.75,min(Cht.Grow.Dev$values),length=0.1,angle=90,lwd=2,col="black",code=3)
	text(cgd.max+2,(max(Cht.Grow.Dev$values)+min(Cht.Grow.Dev$values))/2,round(max(Cht.Grow.Dev$values)-min(Cht.Grow.Dev$values),2),pos=4,cex=0.8,font=2)
	
	#Send plot to PNG;
	if(output=="PNG"){
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","Cht_Grow_Dev.png",sep=""),width=25,height=18,res=300,units="cm",pointsize=14,bg="White")
		plot(as.numeric(as.character(Cht.Grow.Dev$ind)),Cht.Grow.Dev$values,ylab="Cohort Growth Deviation",xlab="Cohort",type="b",lwd=2,ylim=gdevlim,mfrow=c(2,1),col="Blue",xlim=c(cgd.min-1,cgd.max+3)) 
		abline(h=0,lty=3,lwd=1,col="grey")
		arrows(cgd.max+1.75,max(Cht.Grow.Dev$values),cgd.max+1.75,min(Cht.Grow.Dev$values),length=0.1,angle=90,lwd=2,col="black",code=3)
		text(cgd.max+2,(max(Cht.Grow.Dev$values)+min(Cht.Grow.Dev$values))/2,round(max(Cht.Grow.Dev$values)-min(Cht.Grow.Dev$values),2),pos=4,cex=0.8,font=2)
		dev.off()
	}
	
	if(control$Compare[i]==TRUE){
		
		#Plot Species 1 & 2 CGD Compared;
		plot(as.numeric(as.character(Cht.Grow.Dev$ind)),Cht.Grow.Dev$values,ylab="Cohort Growth Deviation",xlab="Cohort",type="b",lwd=2,ylim=gdevlim,mfrow=c(2,1),col="Blue",xlim=c(cgd.min-1,cgd.max+2),main=paste("Cohort Growth Deviation for ",common.name," c.f. ",common.name.2,sep="")) 
		abline(h=0,lty=3,lwd=1,col="grey")
		points(as.numeric(as.character(Cht.Grow.Dev.2$ind)),Cht.Grow.Dev.2$values,type="b",lwd=2,col="Red") 
		arrows(cgd.max+1.75,max(Cht.Grow.Dev$values),cgd.max+1.75,min(Cht.Grow.Dev$values),length=0.1,angle=90,lwd=2,col="Blue",code=3)
		arrows(cgd.max+1,max(Cht.Grow.Dev.2$values),cgd.max+1,min(Cht.Grow.Dev.2$values),length=0.1,angle=90,lwd=2,col="Red",code=3)
		legend("topleft", bty = "n", c(common.name,common.name.2),lty = 1,lwd=2, col = c("Blue","Red"))    

		
		#Send comparison plot to PNG;
		if(output=="PNG"){
			png(paste(out.dir,"/",common.name,"_",common.name.2,"_Cht_Grow_Dev.png",sep=""),width=25,height=18,res=300,units="cm",pointsize=14,bg="White")
			plot(as.numeric(as.character(Cht.Grow.Dev$ind)),Cht.Grow.Dev$values,ylab="Cohort Growth Deviation",xlab="Cohort",type="b",lwd=2,ylim=gdevlim,mfrow=c(2,1),col="Blue",xlim=c(cgd.min-1,cgd.max+2)) 
			abline(h=0,lty=3,lwd=1,col="grey")
			points(as.numeric(as.character(Cht.Grow.Dev.2$ind)),Cht.Grow.Dev.2$values,type="b",lwd=2,col="Red") 
			arrows(cgd.max+1.75,max(Cht.Grow.Dev$values),cgd.max+1.75,min(Cht.Grow.Dev$values),length=0.1,angle=90,lwd=2,col="Blue",code=3)
			arrows(cgd.max+1,max(Cht.Grow.Dev.2$values),cgd.max+1,min(Cht.Grow.Dev.2$values),length=0.1,angle=90,lwd=2,col="Red",code=3)
			legend("topleft", bty = "n", c(common.name, common.name.2),lty = 1,lwd=2, col = c("Blue", "Red"))
			dev.off()
		}
	}
}
	
# PLOT SET 10: CWD for species one and overlayed with CGD for comparison;

if(10 %in% which.plots){
	
	#Plot the CWD for species one;
	plot(as.numeric(as.character(Cht.Weight.Dev$ind)),Cht.Weight.Dev$values,main=paste("Cohort Weight Deviations for ",common.name,sep=""),col="Orange",xlim=c(cgd.min-1,cgd.max+3),pch=1,lwd=2,ylab="Cohort Weight Deviation",xlab="Cohort",type="b",ylim=gdevlim.w)
	abline(h=0,lty=3,col="grey")
	arrows(cgd.max+1.75,max(Cht.Weight.Dev$values),cgd.max+1.75,min(Cht.Weight.Dev$values),length=0.1,angle=90,lwd=2,col="black",code=3)
	text(cgd.max+2,(max(Cht.Weight.Dev$values)+min(Cht.Weight.Dev$values))/2,round(max(Cht.Weight.Dev$values)-min(Cht.Weight.Dev$values),2),pos=4,cex=0.8,font=2)
	
	
	#Send the plot to PNG;
	if(output=="PNG"){
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","Cht_Wt_Dev.png",sep=""),width=25,height=18,res=300,units="cm",pointsize=14,bg="White")
		plot(as.numeric(as.character(Cht.Weight.Dev$ind)),Cht.Weight.Dev$values,col="Orange",xlim=c(cgd.min-1,cgd.max+3),pch=1,lwd=2,ylab="Cohort Weight Deviation",xlab="Cohort",type="b",ylim=gdevlim.w)
		abline(h=0,lty=3,col="grey")
		arrows(cgd.max+1.75,max(Cht.Weight.Dev$values),cgd.max+1.75,min(Cht.Weight.Dev$values),length=0.1,angle=90,lwd=2,col="black",code=3)
		text(cgd.max+2,(max(Cht.Weight.Dev$values)+min(Cht.Weight.Dev$values))/2,round(max(Cht.Weight.Dev$values)-min(Cht.Weight.Dev$values),2),pos=4,cex=0.8,font=2)
		dev.off()
	}

	#Plot CGD in Length for Species One and Overlay with CWD (Weight Deviation);
	plot(as.numeric(as.character(Cht.Grow.Dev$ind)),Cht.Grow.Dev$values,ylab="Cohort Growth Deviation",xlab="Cohort",type="b",lwd=2,ylim=gdevlim.w,xlim=c(cgd.min-1,cgd.max+2),col="Blue",main=paste("CGDevs for ",common.name," - Length vs. Weight ",sep="")) 
	abline(h=0,lty=3,lwd=2,col="dark grey")
	points(as.numeric(as.character(Cht.Weight.Dev$ind)),Cht.Weight.Dev$values,col="Orange",pch=1,lwd=2,type="b")
	arrows(cgd.max+1,max(Cht.Grow.Dev$values),cgd.max+1,min(Cht.Grow.Dev$values),length=0.05,angle=90,lwd=2,col="Blue",code=3)
	arrows(cgd.max+1.75,max(Cht.Weight.Dev$values),cgd.max+1.75,min(Cht.Weight.Dev$values),length=0.05,angle=90,lwd=2,col="Orange",code=3)
	legend("topleft",bty="n",c("Length","Weight"),lty=1,lwd=2,col=c("Blue","Orange"))

	#Send the plot to PNG;
	if(output=="PNG"){
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","CDG_W_L.png",sep=""),width=25,height=18,res=300,units="cm",pointsize=14,bg="White")
		plot(as.numeric(as.character(Cht.Grow.Dev$ind)),Cht.Grow.Dev$values,ylab="Cohort Growth Deviation",xlab="Cohort",type="b",lwd=2,ylim=gdevlim.w,xlim=c(cgd.min-1,cgd.max+2),col="Blue") 
		abline(h=0,lty=3,lwd=2,col="dark grey")
		points(as.numeric(as.character(Cht.Weight.Dev$ind)),Cht.Weight.Dev$values,col="Orange",pch=1,lwd=2,type="b")
		arrows(cgd.max+1,max(Cht.Grow.Dev$values),cgd.max+1,min(Cht.Grow.Dev$values),length=0.05,angle=90,lwd=2,col="Blue",code=3)
		arrows(cgd.max+1.75,max(Cht.Weight.Dev$values),cgd.max+1.75,min(Cht.Weight.Dev$values),length=0.05,angle=90,lwd=2,col="Orange",code=3)
		legend("topleft",bty="n",c("Length","Weight"),lty=1,lwd=2,col=c("Blue","Orange"))
		dev.off()
    }
}
	
# PLOT SET 11: YGD Statistic for species one and overlayed with YWD for comparison;
if(11 %in% which.plots){
	
	#Plot the YDG for species one;
	plot(as.numeric(as.character(Yr.Grow.Dev$ind)),Yr.Grow.Dev$values,ylab="Yearly Growth Deviation",xlab="Sample Year",type="b",lwd=2,ylim=ydevlim,col="Dark Green",main=paste("Yearly Growth Deviation for ",common.name,sep="")) 
	abline(h=0,lty=3,lwd=1,col="grey")

	#Send the plot to PNG;
	if(output=="PNG"){
	png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","Yr_Grow_Dev.png",sep=""),width=25,height=18,res=300,units="cm",pointsize=14,bg="White")
	plot(as.numeric(as.character(Yr.Grow.Dev$ind)),Yr.Grow.Dev$values,ylab="Yearly Growth Deviation",xlab="Sample Year",type="b",lwd=2,ylim=ydevlim,col="Dark Green") 
	abline(h=0,lty=3,lwd=1,col="grey")
	dev.off()
	}
    
	#Plot YGD in Length for Species One and Overlay with YWD (Weight Deviation);
	ygd.min=min(as.numeric(as.character(Yr.Grow.Dev$ind)))
	ygd.max=max(as.numeric(as.character(Yr.Grow.Dev$ind)))

	plot(as.numeric(as.character(Yr.Grow.Dev$ind)),Yr.Grow.Dev$values,ylab="Yearly Growth Deviation",xlab="Sample Year",type="b",lwd=2,ylim=ydevlim.w,xlim=c(ygd.min-1,ygd.max+2),col="Dark Green",main=paste("YGD for ",common.name," - Length vs. Weight ",sep=""))
	abline(h=0,lty=3,lwd=1,col="grey")
	points(as.numeric(as.character(Yr.Weight.Dev$ind)),Yr.Weight.Dev$values,col="Brown",pch=1,lwd=2,type="b")
	arrows(ygd.max+1,max(Yr.Grow.Dev$values),ygd.max+1,min(Yr.Grow.Dev$values),length=0.05,angle=90,lwd=2,col="Dark Green",code=3)
	arrows(ygd.max+1.75,max(Yr.Weight.Dev$values),ygd.max+1.75,min(Yr.Weight.Dev$values),length=0.05,angle=90,lwd=2,col="Brown",code=3)
	legend("topleft",bty="n",c("Length","Weight"),lty=1,lwd=2,col=c("Dark Green","Brown"))

	
	#Send the plot to PNG;
	if(output=="PNG"){
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","YDG_W_L.png",sep=""),width=25,height=18,res=300,units="cm",pointsize=14,bg="White")
		plot(as.numeric(as.character(Yr.Grow.Dev$ind)),Yr.Grow.Dev$values,ylab="Yearly Growth Deviation",xlab="Sample Year",type="b",lwd=2,ylim=ydevlim.w,xlim=c(ygd.min-1,ygd.max+2),col="Dark Green")
		abline(h=0,lty=3,lwd=1,col="grey")
		points(as.numeric(as.character(Yr.Weight.Dev$ind)),Yr.Weight.Dev$values,col="Brown",pch=1,lwd=2,type="b")
		arrows(ygd.max+1,max(Yr.Grow.Dev$values),ygd.max+1,min(Yr.Grow.Dev$values),length=0.05,angle=90,lwd=2,col="Dark Green",code=3)
		arrows(ygd.max+1.75,max(Yr.Weight.Dev$values),ygd.max+1.75,min(Yr.Weight.Dev$values),length=0.05,angle=90,lwd=2,col="Brown",code=3)
		legend("topleft",bty="n",c("Length","Weight"),lty=1,lwd=2,col=c("Dark Green","Brown"))
		dev.off()
	}
 }

#PLOT/ANALYSIS 12: VonBert growth equations changing over time (retrospective style analysis over X year increments);
if(12 %in% which.plots){

	#Set start year, number of years since start of data series for first calculation, and number of years over which to repeat the calculations;
	start.year=1999 #First year for the X previous years method;
	first.years=10	#Time since min data year to start pooled data method;
	block.years=5	#Number of years over which to repeat the model fit;
	
	#PART A:
	
	#Plot VonBert growth equations differing over time (previous X years of data method) *Remember selectivity not accounted for here;
	plot(data.laa.plot$Age,data.laa.plot$Length,pch=1,xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",col="grey",main=paste("Change to estimated growth model over time for ",common.name,sep=""))

	VBParms.1=data.frame(Year=0,Li=0,K=0,t0=0)	#Create a data frame for recording von Bert Parms;

	for(i in seq(start.year,max(data.la$Year),block.years)){
		data.data=data.la[data.la$Year %in% (i-block.years):(i-1) ,]
		
			amax=max(data.la$Age)
			lmax=max(data.la$Length)
			Li=lmax-20					#Starting values for VB model fitting;
			K=0.2
			t0=0
			vb.model.vt=nls(VonBert,start=list(Li=Li,K=K,t0=t0),data=data.data)
			xv=seq(1,amax,0.1)
			yv=predict(vb.model.vt,list(Age=xv))
			lines(xv,yv,col=i,lty=1,lwd=3)
			
			L=VBLen(Age=0:amax,Li=coef(vb.model.vt)["Li"],k=coef(vb.model.vt)["K"],t0=coef(vb.model.vt)["t0"])
			
			if(i==min(seq(start.year,max(data.la$Year),block.years))){
				LAA.1=data.frame(Age=0:amax)		#Create a data frame for recording length-at-age values as they change through time;
		}
		LAA.1=cbind(LAA.1,L)
		names(LAA.1)[length(LAA.1)]=i
		
		Parms=c(i,coef(vb.model.vt)["Li"],coef(vb.model.vt)["K"],coef(vb.model.vt)["t0"])
		VBParms.1=rbind(VBParms.1,Parms)
	}
	
	legend(x="bottomright",legend=as.factor(seq(start.year,max(data.la$Year),block.years)),col=seq(start.year,max(data.la$Year),block.years),lty=1,lwd=2,bty ="n")
	text(0,0,labels=paste("*Calculated in specified year, with data from previous", block.years, "years",sep=" "),pos=4,cex=0.8)
	
	VBParms.1=VBParms.1[-1,]	#Remove zero starting row from VBParms data frame;
	print(VBParms.1)
	
	print(LAA.1)
	
	
	#Send plot to PNG;
	if(output=="PNG"){	
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","Retrospective_Growth_",block.years,"_Yrs_Data.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="White")
		
		plot(data.laa.plot$Age,data.laa.plot$Length,pch=1,xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",col="grey")

		for(i in seq(start.year,max(data.la$Year),block.years)){
		data.data=data.la[data.la$Year %in% (i-block.years):(i-1) ,]
		
			amax=max(data.la$Age)
			lmax=max(data.la$Length)
			Li=lmax-20					#Starting values for VB model fitting;
			K=0.2
			t0=0
			vb.model.vt=nls(VonBert,start=list(Li=Li,K=K,t0=t0),data=data.data)
			xv=seq(1,amax,0.1)
			yv=predict(vb.model.vt,list(Age=xv))
			lines(xv,yv,col=i,lty=1,lwd=3)
		}
	
	  legend(x="bottomright",legend=as.factor(seq(start.year,max(data.la$Year),block.years)),col=seq(start.year,max(data.la$Year),block.years),lty=1,lwd=2,bty ="n")
	  text(0,0,labels=paste("*Calculated in specified year, with data from previous", block.years, "years",sep=" "),pos=4,cex=0.8)
    
	  write.table(round(VBParms.1,2),paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","Retrospective_Growth_VBParms_",block.years,"_Yrs_Data.txt",sep=""),quote=FALSE,row.names=FALSE)
	
	  write.table(round(LAA.1,2),paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","Retrospective_Growth_LAA_",block.years,"_Yrs_Data.txt",sep=""),quote=FALSE,row.names=FALSE)
		
	dev.off()	
	}
	
	#PART B:
	
	#Plot VonBert growth equations differing over time (every X years with pooled data method) *Remember selectivity not accounted for here;
	plot(data.laa.plot$Age,data.laa.plot$Length,pch=1,xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",col="grey",main=paste("Change to estimated growth model over time for ",common.name,sep=""))

	VBParms.2=data.frame(Year=0,Li=0,K=0,t0=0)	#Create a data frame for recording von Bert Parms;

	for(i in seq(min(data.la$Year)+first.years,max(data.la$Year),block.years)){
		data.data=data.la[data.la$Year %in% min(data.la$Year):(i-1) ,]
		
		amax=max(data.la$Age)
		lmax=max(data.la$Length)
		Li=lmax-20					#Starting values for VB model fitting (good for BG, maybe not for other species?);
		K=0.2
		t0=0
		vb.model.vt=nls(VonBert,start=list(Li=Li,K=K,t0=t0),data=data.data)
		xv=seq(1,amax,0.1)
		yv=predict(vb.model.vt,list(Age=xv))
		lines(xv,yv,col=i,lty=1,lwd=3)
		
		L=VBLen(Age=0:amax,Li=coef(vb.model.vt)["Li"],k=coef(vb.model.vt)["K"],t0=coef(vb.model.vt)["t0"])
		
		if(i==min(seq(min(data.la$Year)+first.years,max(data.la$Year),block.years))){
			LAA.2=data.frame(Age=0:amax)		#Create a data frame for recording length-at-age values as they change through time;
		}
		LAA.2=cbind(LAA.2,L)
		names(LAA.2)[length(LAA.2)]=i
		
		Parms=c(i,coef(vb.model.vt)["Li"],coef(vb.model.vt)["K"],coef(vb.model.vt)["t0"])
		VBParms.2=rbind(VBParms.2,Parms)
	}
	
	legend(x="bottomright",legend=as.factor(seq(min(data.la$Year)+first.years,max(data.la$Year),block.years)),col=seq(min(data.la$Year)+first.years,max(data.la$Year),block.years),lty=1,lwd=2,bty ="n")
	text(0,0,labels="*Calculated in specified year, with data from all previous years",pos=4,cex=0.8)
	
	VBParms.2=VBParms.2[-1,]	#Remove zero starting row from VBParms data frame;
	print(VBParms.2)
	
	print(LAA.2)
	
	
	#Send plot to PNG;
	if(output=="PNG"){	
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","Retrospective_Growth_DataPooled.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="White")
		
		plot(data.laa.plot$Age,data.laa.plot$Length,pch=1,xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",col="grey")

		for(i in seq(min(data.la$Year)+first.years,max(data.la$Year),block.years)){
			data.data=data.la[data.la$Year %in% min(data.la$Year):(i-1) ,]
			
			amax=max(data.la$Age)
			lmax=max(data.la$Length)
			Li=lmax-20					#Starting values for VB model fitting (good for BG, maybe not for other species?);
			K=0.2
			t0=0
			vb.model.vt=nls(VonBert,start=list(Li=Li,K=K,t0=t0),data=data.data)
			xv=seq(1,amax,0.1)
			yv=predict(vb.model.vt,list(Age=xv))
			lines(xv,yv,col=i,lty=1,lwd=3)
		}
	
	
      write.table(round(VBParms.2,2),paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","Retrospective_Growth_VBParms_DataPooled.txt",sep=""),quote=FALSE,row.names=FALSE)
	
	  write.table(round(LAA.2,2),paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","Retrospective_Growth_LAA_DataPooled.txt",sep=""),quote=FALSE,row.names=FALSE)
		
	  legend(x="bottomright",legend=as.factor(seq(min(data.la$Year)+first.years,max(data.la$Year),block.years)),col=seq(min(data.la$Year)+first.years,max(data.la$Year),block.years),lty=1,lwd=2,bty ="n")
	  text(0,0,labels="*Calculated in specified year, with data from all previous years",pos=4,cex=0.8)
	
	dev.off()
	}
}


#PLOT 13: Length and mean-length data per cohort of each age group (specify cohorts and ages of interest);
if(13 %in% which.plots){

	#Observed length data for age groups;
	#Create new data frame with extra column for name of age group;
	data.la.a=cbind(data.la,Years.Old=paste(data.la$Age," Year Olds"))
	
	#Create some objects useful for plotting;
	mina=floor(mean(data.la.a$Age)-sd(data.la.a$Age))
	maxa=ceiling(mean(data.la.a$Age)+sd(data.la.a$Age))
	anum=maxa-mina+1
	aplotx=round(sqrt(anum))
	aploty=ifelse((sqrt(anum)-floor(sqrt(anum)))<=0.5 & (sqrt(anum)-floor(sqrt(anum)))!=0 ,aplotx+1,aplotx)
	
	#For presentation plots (if present==TRUE)
	if(present==TRUE){
		mina=3
		maxa=8
		anum=maxa-mina+1
		aplotx=round(sqrt(anum))
		aploty=ifelse((sqrt(anum)-floor(sqrt(anum)))<=0.5 & (sqrt(anum)-floor(sqrt(anum)))!=0 ,aplotx+1,aplotx)
		}
		
	#Create data frame with cohorts of interest and some objects ready for plotting;
	data.la.a=data.la.a[data.la.a$Cohort>=cmin&data.la.a$Cohort<=cmax&data.la.a$Age>=mina&data.la.a$Age<=maxa,]
	data.la.a.plot=unique(data.la.a)
	lmin.a=min(data.la.a$Length)-10
	lmax.a=max(data.la.a$Length)+10
	
	#Create plotting function using xyplot;
	laa.plot.a<-function(main){print(xyplot(Length~Cohort|Years.Old,pch=23,xlim=c(cmin-2,cmax+4),ylim=c(lmin.a,lmax.a),xlab="Cohort",ylab="Length(cm)",between=list(x=0.3,y=0.3),as.table=T,layout=c(aplotx,aploty),data=data.la.a.plot,main=main))}
	
	#Print plot to screen;
	laa.plot.a(main=paste("Observed Length Data by Age Groups for ",common.name,sep=""))
	  
	#Save plot to PNG;
	if(output=="PNG"){
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","Lattice_Length_Ages.png",sep=""),width=25,height=20,res=300,units="cm",pointsize=14,bg="White")
		laa.plot.a(main=NULL)
		dev.off()
	}

	#Mean-length per cohort for age groups, (with error bars);
	#Create new data frame with extra column for name of age group;
	data.cmla.b=cbind(data.cmla,Years.Old=paste(data.cmla$Age," Year Olds",sep=""))
	
	#Create data frame with cohorts of interest and some objects ready for plotting;
	data.cmla.b=data.cmla.b[data.cmla.b$Cohort>=cmin&data.cmla.b$Cohort<=cmax&data.cmla.b$Age>=mina&data.cmla.b$Age<=maxa,]
	lmin.b=min(data.cmla.b$Mean.Length)-10
	lmax.b=max(data.cmla.b$Mean.Length)+10
	
	#Create plotting function using xYplot;
	mlaa.plot<-function(main){print(xYplot(Cbind(Mean.Length,Mean.Length-1.96*SEM,Mean.Length+1.96*SEM)~Cohort|Years.Old,pch=18,type="b",
		xlim=c(cmin-2,cmax+4),ylim=c(lmin.b,lmax.b),xlab="Cohort",ylab="Length(cm)",between=list(x=0.3,y=0.3),
		as.table=T,layout=c(aplotx,aploty),data=data.cmla.b,main=main))}

	#Print plot to screen;
	mlaa.plot(main=paste("Observed Mean-Length by Age Groups for ",common.name,sep=""))
	
	#Save plot to PNG;
	if(output=="PNG"){
		png(paste(out.dir,"/",common.name,"_",gear,"_",zone,"_","Lattice_Mean_Length_Ages_Thesis.png",sep=""),width=25,height=18,res=300,units="cm",pointsize=14,bg="White")
		mlaa.plot(main=NULL)
		dev.off()
	}
}

}
	
###########################################
# END of PLOTS SECTION, END OF SCRIPT.
###########################################

