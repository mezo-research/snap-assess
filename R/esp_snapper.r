#####################################################################################################################
#
# 	esp_snapper.r
#	R code implementing selectivity estimation functions defined in espf.r
#	Plots and analyses of outputs for snapper Data.
#	
# 	Code by Athol Whitten (athol.whitten@mezo.com.au)
# 	Melbourne, Australia, 2011
#
#	Created: 16th May 2012
#	Updated: 19th November 2012
#
#######################################################################################################################

#Remove all objects and lists currently in workspace as precaution:
rm(list=ls())

#Specify directory of working folder;
main.folder <- "C:/Dropbox/Mezo/Projects/Snapper"

#Specify folder for printing plots of results (set here to be a subfolder of the working folder, and named 'Output'):
output.folder <- paste(main.folder,"/Output",sep="")
dir.create(output.folder)

#Source the requisite functions from the espf.r file:
source(paste(main.folder,"/Scripts/espf.r",sep=""))

#Get the data and set the gsizes (gear sizes) vector (snapper used here as an example):
all.data <- (read.csv(paste(main.folder,"/Data/FV_Raw.csv",sep=""),header=TRUE))
snapper.data <- all.data[all.data$Species == "Snapper",]

dim(snapper.data)
summary(snapper.data)

ppb.snapper.data <- snapper.data[snapper.data$Location == "PPB",]

dim(ppb.snapper.data)
summary(ppb.snapper.data)

plot(ppb.snapper.data$Age,ppb.snapper.data$FL)
 
snapper.gsizes <- c()

#View and check the data and gear size vector:
print(snapper.data)
print(snapper.gsizes)

#Set initial parameter values for Theta 1 through Theta 4;
net.parms <- c() #Theta 1 and 2.
gc.parms <- c(2,0.5) #Theta 3 and 4.

#Implement optimisation with all data, using Nelder-Mead method as part of R optim function:
snapper.fit <- optim(c(gn.parms,gc.parms),esp.nll,cdata=snapper.data,gsizes=snapper.gsizes,sel.a=esp.gamma,sel.b=esp.lnorm,hessian=TRUE,method="Nelder-Mead")

#Get parameter estimates and the mode and variance estimates for the first two nets and the Cormorants;
snapper.est <- esp.est(fit=snapper.fit,gsizes=snapper.gsizes)

#Get plots of base data (barplot) and of the estimated selectivity curves over a specified range of plot lengths;
esp.plot(fit=snapper.fit,gsizes=snapper.gsizes,cdata=snapper.data,sel.a=esp.gamma,sel.b=esp.lnorm,BS=TRUE,plot.lens=seq(0.0001,50,0.01),label=TRUE,save=TRUE,save.to=output.folder,name="snapper")

#######################################################################################################################
# End of esp.r.
#######################################################################################################################