#--------------------------------------------------------------------------------------
#
# *catch_snapper.r reformats, summarises, analyses, and plots catch data 
# for Pink Snapper from Fisheries Victoria, for pre-assessment analyses.
#
# Output files are sent to the ~/Snapper/Output directory.
#
# By Athol R. Whitten and Simone R. Stuckey, Mezo Research Pty. Ltd.
# Contact: athol.whitten@mezo.com.au, Last Updated: October 2013
#
#--------------------------------------------------------------------------------------

#Remove previous R console object assignments and load required libraries;
rm(list=ls())
library(plyr)
library(xtable)

#-------------------------------------
# Set and load directories/files:
#-------------------------------------

#Set root directory:
rootdir <- "C:/Dropbox"
working.folder <- "C:/Dropbox/Fisheries/DEPI/Assessment"

#Set directory to relevant files:
species <- "Snapper"
assess.year <- 2013
data.year <- 2013
user.name <- "Athol Whitten"
data.dir <- paste(working.folder, assess.year, species, "Data", sep="/")
out.dir <- paste(working.folder, assess.year, species, "Output", sep="/")

ca.data.rec <- "/MZ_Catch_Snapper_Recreational.csv"
ca.data.com <- "/MZ_Catch_Snapper_Commercial.csv"

#Source some functions for plot diagnotics:
source(paste(rootdir, "/Modelling/R/Source/FishFunc.r",sep=""))

#Read Data (.csv) Files:
data.ca.com.raw <- read.csv(paste(data.dir, ca.data.com, sep=""), header=TRUE, comment.char="")
data.ca.rec.raw <- read.csv(paste(data.dir, ca.data.rec, sep=""), header=TRUE, comment.char="")