#--------------------------------------------------------------------------------------
#
# *len_snapper.r reformats, summarises, analyses, and plots length data 
# for Pink Snapper from Fisheries Victoria, for pre-assessment analyses.
#
# Output files are sent to the ~/Snapper/Output directory.
#
# By Athol R. Whitten and Simone R. Stuckey, Mezo Research Pty. Ltd.
# Contact: athol.whitten@mezo.com.au, Last Updated: December 2013
#
#--------------------------------------------------------------------------------------

# Load libraries if required:
library(plyr)

#-------------------------------------
# Set and load directories/files:
#-------------------------------------

# Set root directory:
rootdir <- "C:/Dropbox"
working.folder <- "/Users/awhitten/Mezo/Business/Projects/Marine/Snapper_Assess"

# Set directory to relevant files:
species <- "Snapper"
assess.year <- 2016
data.year <- 2016
user.name <- "Athol Whitten"
data.dir <- paste(working.folder, assess.year, species, "Data", sep="/")
out.dir <- paste(working.folder, assess.year, species, "Output", sep="/")

len.data.rec <- "/MZ_Length_Recreational_Snapper.csv"
len.data.com <- "/MZ_Length_Commercial_Snapper.csv"

# Set parameters for length conversion formulae (where FL = a*TL - b):
len.a <- 0.888
len.b <- 1.25

# Source some functions for plot diagnostics:
source(paste(rootdir, "/Modelling/R/Source/FishFunc.r",sep=""))

# Read Data (.csv) Files:
data.len.rec.raw <- read.csv(paste(data.dir, len.data.rec, sep=""), header=TRUE, comment.char="")
data.len.com.raw <- read.csv(paste(data.dir, len.data.com, sep=""), header=TRUE, comment.char="")

#-----------------------------------------------
# Reformat and configure data for analyses:
#-----------------------------------------------

# Create new data frames (clean if required):
data.len.rec <- data.len.rec.raw
data.len.com <- data.len.com.raw[,c("Year","Month","Zone","Length","Count","Gear.Code")]

# Convert 'Total Length' data in Recreational Length Comp Data to 'Fork Length' and add new column for 'Length':
total.lengths <- data.len.rec$Total.Length[which(data.len.rec$Length.Type == "Total")]
fork.lengths <- round((len.a*total.lengths) - len.b)
data.len.rec$Fork.Length[which(data.len.rec$Length.Type == "Total")] <- fork.lengths
data.len.rec$Length <- data.len.rec$Fork.Length

# Remove rows where length is zero or NA (assumed these values have been recorded in error):
data.len.rec <- data.len.rec[-which(is.na(data.len.rec$Fork.Length)), ]
data.len.rec <- data.len.rec[data.len.rec$Fork.Length>0, ]
data.len.com <- data.len.com[data.len.com$Length>0, ]

# Create 'Gear.Group' column for fishing 'fleet' categoies for assessment:
data.len.com$Gear.Group <- NA
data.len.com$Gear.Group[which(data.len.com$Gear.Code == "LL")] <- "LL"
data.len.com$Gear.Group[which(data.len.com$Gear.Code == "HS")] <- "SS"
data.len.com$Gear.Group[which(data.len.com$Gear.Code == "M1")] <- "SS"
data.len.com$Gear.Group[which(data.len.com$Gear.Code == "M2")] <- "SS"
data.len.com$Gear.Group[which(data.len.com$Gear.Code == "M4")] <- "SS"
data.len.com$Gear.Group[which(data.len.com$Gear.Code == "M5")] <- "SS"
data.len.com$Gear.Group[which(data.len.com$Gear.Code == "PS")] <- "SS"
data.len.com$Gear.Group <- as.factor(data.len.com$Gear.Group)

#---------------------------------------------------------
# Split data into data frames specific for each fleet:
#---------------------------------------------------------

# Create 'Fleet' Column for sorting data and formatting for SS:
data.len.com$Fleet <- NA
data.len.com$Fleet[which(data.len.com$Zone == "PPB" &  data.len.com$Gear.Group == "LL")] <- 1
data.len.com$Fleet[which(data.len.com$Zone == "PPB" &  data.len.com$Gear.Group == "SS")] <- 2

data.len.rec$Fleet <- NA
data.len.rec$Fleet[which(data.len.rec$Zone == "PPB" & data.len.rec$Season.CDP == "Pinky")] <- 4
data.len.rec$Fleet[which(data.len.rec$Zone == "PPB" & data.len.rec$Season.CDP == "Adult")] <- 5

# Remove data not correspodning to one of above fleets:
data.len.com <- data.len.com[-which(is.na(data.len.com$Fleet)),]
data.len.rec <- data.len.rec[-which(is.na(data.len.rec$Fleet)),]

# Create data frames for PPB Longline, Seine Fleet, Pinky, and Adult :
data.len.com.ll <- data.len.com[which(data.len.com$Fleet == 1),]
data.len.com.ss <- data.len.com[which(data.len.com$Fleet == 2),]
data.len.rec.pny <- data.len.rec[which(data.len.rec$Fleet == 4),]
data.len.rec.adt <- data.len.rec[which(data.len.rec$Fleet == 5),]

#---------------------------------------------------------
# Set desired bin size and create column for length bin:
#---------------------------------------------------------

# Set desired bin size:
bin.size <- 2

# Determine vector of length bins, lower bounds (break points), given bin size:
min.bin.rec <- floor(min(data.len.rec$Length) / bin.size) * bin.size
max.bin.rec <- floor(max(data.len.rec$Length) / bin.size) * bin.size
min.bin.com <- floor(min(data.len.com$Length) / bin.size) * bin.size
max.bin.com <- floor(max(data.len.com$Length) / bin.size) * bin.size

min.bin <- min(c(min.bin.rec,min.bin.com))
max.bin <- max(c(max.bin.rec,max.bin.com))
bin.vector <- seq(min.bin,max.bin,bin.size)

# Add column to categorise length data into length bins (cut by bin vector):
data.len.rec$Length.Bin <- cut(data.len.rec$Length, breaks=bin.vector, labels=FALSE, include.lowest=TRUE)
data.len.com$Length.Bin <- cut(data.len.com$Length, breaks=bin.vector, labels=FALSE, include.lowest=TRUE)

# Expand Commercial length data frame (from count data for length records repeated) by repeating those lines, (count) number of times:
data.len.com <- data.len.com[rep(seq_len(nrow(data.len.com)), data.len.com$Count),]
data.len.com <- subset(data.len.com, select=-Count)

#---------------------------------------------------------
# Reformat flat data file into frequency table for SS:
#---------------------------------------------------------

# Get frequency proportion table for commercial data:
freq.data.len.com <- data.frame()

for(i in unique(data.len.com$Fleet)){
		
		data.len.com.fleet 		<- data.len.com[which(data.len.com$Fleet==i),]
		count.table.com.fleet 	<- table(Year=data.len.com.fleet$Year, Length.Bin=data.len.com.fleet$Length.Bin) 
		samples 				<- as.vector(apply(count.table.com.fleet[,-1],1,sum))
		freq.table.com.fleet 	<- round(prop.table(count.table.com.fleet,1),6)
		freq.table.com.frame  	<- as.data.frame(freq.table.com.fleet)
		stack.frame 			<- reshape(freq.table.com.frame, idvar="Year", timevar = "Length.Bin", direction = "wide")
		
		stack.frame$Fleet <- i
		stack.frame$Samples <- samples
		freq.data.len.com <- rbind.fill(freq.data.len.com, stack.frame)
	}
	
freq.data.len.com[is.na(freq.data.len.com)] <- 0

# Get frequency proportion table for recreational data:	
freq.data.len.rec <- data.frame()

for(i in unique(data.len.rec$Fleet)){
		
		data.len.rec.fleet 		<- data.len.rec[which(data.len.rec$Fleet==i),]
		count.table.rec.fleet 	<- table(Year=data.len.rec.fleet$Year, Length.Bin=data.len.rec.fleet$Length.Bin) 
		samples 				<- as.vector(apply(count.table.rec.fleet[,-1],1,sum))
		freq.table.rec.fleet 	<- round(prop.table(count.table.rec.fleet,1),6)
		freq.table.rec.frame 	<- as.data.frame(freq.table.rec.fleet)
		stack.frame 			<- reshape(freq.table.rec.frame, idvar="Year", timevar = "Length.Bin", direction = "wide")
		
		stack.frame$Fleet <- i
		stack.frame$Samples <- samples
		freq.data.len.rec <- rbind.fill(freq.data.len.rec, stack.frame)
	}

freq.data.len.rec[is.na(freq.data.len.rec)] <- 0

# Join the commercial and recreational tables together:
ss.lf.table <- rbind.fill(freq.data.len.com, freq.data.len.rec)
ss.lf.table[is.na(ss.lf.table)] <- 0

# Reorder columns and rename with bin vector:
fleet.col <- which(names(ss.lf.table)=="Fleet")
ss.lf <- ss.lf.table[c(1,fleet.col:ncol(ss.lf.table),2:(fleet.col-1))]
names(ss.lf)[4:ncol(ss.lf)] <- bin.vector[-length(bin.vector)]

# Write the data.frame to file:
write.table(ss.lf, paste(out.dir,"SS_Length_Freq_Table.txt",sep="/"), row.names=FALSE)

# Check how the breaks=bin.vector works and which values are going where
# Check if the values entered to SS should be the mid-points, and change the names accordingly, as these can be used as the .dat file length bin vector.

# NOTE IMPORTANT: Need to do the same sort of formatting for conditional age-length data. 
# Need to split this data by sex, and provide column which identifies sex per record (as available for age data but not length).

#-----------------------------------------------------
# Count data by Area, Gear, etc. and Capture Tables
# NOTE: Currently not seperated into fleets
#-----------------------------------------------------

# Create, print, and save area table for recreational fishery:
len.area.count.rec <- aggregate(data.len.rec.ppb$Area, list(Area=data.len.rec.ppb$Area), length)
names(len.area.count.rec)[2] <- "Count"
len.area.count.rec <- len.area.count.rec[rev(order(len.area.count.rec$Count)), ]
row.names(len.area.count.rec) <- NULL
print(len.area.count.rec)

last.area <- 16
len.area.count.rec <- rbind(len.area.count.rec[1:last.area, ], data.frame("Area"="Other","Count"=sum(len.area.count.rec[(last.area+1):(nrow(len.area.count.rec)),"Count"])))
print(len.area.count.rec)

len.area.table.rec <- xtable(len.area.count.rec,caption=paste("Number of length samples (recreational fishery) of", species, "per ramp sampling area for", len.rec.span, sep=" "),label="table:len.area.count.rec")
file.name <- paste(out.dir, "/Area_Count_Length_Samples_Rec_", data.year, ".tex", sep="")
file.create(file.name,overwrite=T)
capture.output(print(len.area.table.rec, include.rownames=FALSE), file=file.name)

#-------------------------------------------------------
# Make summary objects for data analyses and plotting:
#-------------------------------------------------------

# Get start and end years (and year spans) for each data set:
len.com.start.ll <- min(data.len.com.ll$Year)
len.com.end.ll <- max(data.len.com.ll$Year)
len.com.span.ll <- paste(as.character(len.com.start.ll), "-", as.character(len.com.end.ll), sep = " ")

len.com.start.ss <- min(data.len.com.ss$Year)
len.com.end.ss <- max(data.len.com.ss$Year)
len.com.span.ss <- paste(as.character(len.com.start.ss), "-", as.character(len.com.end.ss), sep = " ")

len.rec.start.pny <- min(data.len.rec.pny$Year)
len.rec.end.pny <- max(data.len.rec.pny$Year)
len.rec.span.pny <- paste(as.character(len.rec.start.pny), "-", as.character(len.rec.end.pny), sep = " ")

len.rec.start.adt <- min(data.len.rec.adt$Year)
len.rec.end.adt <- max(data.len.rec.adt$Year)
len.rec.span.adt <- paste(as.character(len.rec.start.adt), "-", as.character(len.rec.end.adt), sep = " ")

# NOTE: May be able to match this with SS data bins
# Get min and max lengths (and thus number of bins) for each data set:
len.com.min.ll <- min(data.len.com.ll$Length)
len.com.max.ll <- max(data.len.com.ll$Length)
len.com.bins.ll <- len.com.max.ll - len.com.min.ll

len.com.min.ss <- min(data.len.com.ss$Length)
len.com.max.ss <- max(data.len.com.ss$Length)
len.com.bins.ss <- len.com.max.ss - len.com.min.ss

len.rec.min.pny <- min(data.len.rec.pny$Length)
len.rec.max.pny <- max(data.len.rec.pny$Length)
len.rec.bins.pny <- len.rec.max.pny - len.rec.min.pny

len.rec.min.adt <- min(data.len.rec.adt$Length)
len.rec.max.adt <- max(data.len.rec.adt$Length)
len.rec.bins.adt <- len.rec.max.adt - len.rec.min.adt

#-------------------------------------------------
# Plot length frequency distributions
#-------------------------------------------------

# Plot length frequency distribution of snapper caught by long line fleet:
hist.len.com.ll <- hist(data.len.com.ll$Length, main=NULL, xlab="Fork Length (cm)", ylab="Number", cex.lab=1.2, breaks=len.com.bins.ll, col="Light Blue")

# Plot length frequency distribution of snapper caught by haul seine fleet:
hist.len.rec.ss <- hist(data.len.com.ss$Length, main=NULL, xlab="Fork Length (cm)", ylab="Number", cex.lab=1.2, len.com.bins.ss, col="Light Green")

#Save plots to output folder in PNG format:
png(paste(out.dir,"/","Length_Freq_Dist_PPB_LL.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
hist.len.com.ll <- hist(data.len.com.ll$Length, main=NULL, xlab="Fork Length (cm)", ylab="Number", cex.lab=1.2, breaks=len.com.bins.ll, col="Light Blue")
dev.off()

png(paste(out.dir,"/","Length_Freq_Dist_PPB_SS.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
hist.len.rec.ss <- hist(data.len.com.ss$Length, main=NULL, xlab="Fork Length (cm)", ylab="Number", cex.lab=1.2, len.com.bins.ss, col="Light Green")
dev.off()

# Plot length frequency distribution of snapper caught by rod and reel (RR) (aggregated, and then seperately by pinky and adult fleets):
hist.len.rec.ppb <- hist(data.len.rec.ppb$Length, main=NULL, xlab="Fork Length (cm)", ylab="Number", cex.lab=1.2, breaks=len.rec.bins, col="Light Brown")
hist.len.rec.pny <- hist(data.len.rec.pny$Length, main=NULL, xlab="Fork Length (cm)", ylab="Number", cex.lab=1.2, breaks=len.rec.bins.pny, col="Pink")
hist.len.rec.adt <- hist(data.len.rec.adt$Length, main=NULL, xlab="Fork Length (cm)", ylab="Number", cex.lab=1.2, breaks=len.rec.bins.adt, col="Light Blue")

# Save plots to output folder in PNG format:
# Aggregated Length Freq (RR):
png(paste(out.dir,"/","Length_Freq_Dist_PPB_RR.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
hist(data.len.rec.ppb$Fork.Length, main=NULL, xlab="Fork Length (cm)", ylab="Number", cex.lab=1.2, breaks=len.rec.bins, col="Light Brown")
dev.off()

# Pinky Fishery Length Freq (RR):
png(paste(out.dir,"/","Length_Freq_Dist_PPB_RR_Pinkys.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
hist.len.rec.pny <- hist(data.len.rec.pny$Length, main=NULL, xlab="Fork Length (cm)", ylab="Number", cex.lab=1.2, breaks=len.rec.bins.pny, col="Pink")
dev.off()

# Adult Fishery Length Freq (RR):
png(paste(out.dir,"/","Length_Freq_Dist_PPB_RR_Adults.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
hist.len.rec.adt <- hist(data.len.rec.adt$Length, main=NULL, xlab="Fork Length (cm)", ylab="Number", cex.lab=1.2, breaks=len.rec.bins.adt, col="Light Blue")
dev.off()

#FIX CODE BELOW HERE:

#Make a plot of length-frequency distribution of snapper (ALL GEARS) from Port Phillip Bay:
hist.age.ppb <- hist(data.len.ppb$Length, main=NULL, xlab="Length", ylab="Number", cex.lab=1.2, freq=TRUE, breaks=seq(0,max(data.la.ppb$Length)+2,2))

#Save plot to output folder in PNG format
png(paste(out.dir,"/","Length_Freq_Dist_PPB.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
hist.age.ppb <- hist(data.len.ppb$Length, main=NULL, xlab="Length", ylab="Number", cex.lab=1.2, freq=TRUE, breaks=seq(0,max(data.la.ppb$Length)+2,2),col="Pink")
dev.off()

#Make a plot of the season distribution:
hist.len.rec.ppb <- hist(data.len.rec.ppb$Month, main=NULL, xlab="Month", ylab="Number", cex.lab=1.2, breaks=12, col="Light Orange")

#----------------------
# EOF.