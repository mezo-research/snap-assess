#--------------------------------------------------------------------------------------
# run_snapper.R is a wrapper script to run the Snapper ss3 model.
# Output files are sent to the ~/Snapper/Output directory.
#
# By Athol R. Whitten, Mezo Research Pty. Ltd.
# Contact: athol.whitten@mezo.com.au, Last Updated: August 2017
#--------------------------------------------------------------------------------------

# Run Snapper ss3 model
# Use R to run the ss3 model for snapper.
# Use R for clean_up functions etc.
# Use R to run model, then get output plots using r4ss.

# Use Rmd to make a simple report document for snapper too. 
# Set up so that the report can be generated automatically using an R script.
# Use the basic outputs for a template and show that they can be updated easily with Rmd.
# Set Rmd to create a Word Document that Paul can update as required.