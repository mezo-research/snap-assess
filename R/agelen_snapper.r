#--------------------------------------------------------------------------------------
#
# *agelen_snapper.r reformats, summarises, analyses, and plots age-length data 
# for Pink Snapper from Fisheries Victoria, for pre-assessment analyses.
#
# Output files are sent to the ~/Snapper/Output directory.
#
# By Athol R. Whitten and Simone R. Stuckey, Mezo Research Pty. Ltd.
# Contact: athol.whitten@mezo.com.au, Last Updated: October 2013
#
#--------------------------------------------------------------------------------------

#Remove previous R console object assignments and load required libraries;
rm(list=ls())
library(plyr)
library(xtable)
library(gtools)

#-------------------------------------
# Set and load directories/files:
#-------------------------------------

#Set root directory:
rootdir <- "C:/Dropbox"
working.folder <- "C:/Dropbox/Fisheries/DEPI/Assessment"

#Set directory to relevant files:
species <- "Snapper"
assess.year <- 2013
data.year <- 2013
user.name <- "Athol Whitten"
data.dir <- paste(working.folder, assess.year, species, "Data", sep="/")
out.dir <- paste(working.folder, assess.year, species, "Output", sep="/")

la.data <- "/MZ_AgeLen_Snapper.csv"
la.data.batch <- "/MZ_AgeLen_Snapper_Batch_Record.csv"

#Source some functions for plot diagnotics:
source(paste(rootdir, "/Modelling/R/Source/FishFunc.r",sep=""))

#Read Data (.csv) Files:
data.la.raw <- read.csv(paste(data.dir, la.data, sep=""), header=TRUE, comment.char="")
data.la.batch.raw <- read.csv(paste(data.dir, la.data.batch, sep=""), header=TRUE, comment.char="")

#-----------------------------------------------
# Reformat and configure data for analyses:
#-----------------------------------------------

#Create new data frames;
data.la <- data.la.raw
data.la.batch <- data.la.batch.raw

#Merge data.la with other information (inc. gear) from length/age batch record data file:
data.la <- join(data.la, data.la.batch, by = c("Code","Date","Zone","Area","Batch"))

#Add columns for 'Year' and 'Month', by extracting information from 'Date', as numeric values;
data.la <- cbind(data.la, Year=as.numeric(format(as.Date(data.la$Date), "%Y")))
data.la <- cbind(data.la, Month=as.numeric(format(as.Date(data.la$Date), "%m")))

# Add columns for Cohort, Cohort Name and Sample Year Name:
data.la <- cbind(data.la, Cohort=as.numeric(data.la$Year - data.la$Age))
data.la <- cbind(data.la, Cohort.Name=paste("YC",data.la$Cohort), Sample.Year=paste("Year",data.la$Year))

#Remove rows where length is zero or NA (assumed these values have been recorded in error):
data.la <- data.la[data.la$Length>0, ]
if(sum(is.na(data.la$Length)) > 0) data.la <- data.la[-which(is.na(data.la$Length)), ]

#Remove rows where age is zero:
data.la <- data.la[data.la$Age>0, ]
if(sum(is.na(data.la$Age)) > 0) data.la <- data.la[-which(is.na(data.la$Age)), ]

# Combine sexes 3 and 4, into category 3, remove erroneous values (>4), remove NAs:
data.la$Sex[which(data.la$Sex==4)] <- 3
data.la <- data.la[-which(data.la$Sex>3),]
if(sum(is.na(data.la$Sex)) > 0) data.la <- data.la[-which(is.na(data.la$Sex)), ]

#Create dataframes for data for Males (1) and Females (2) and Unknown (3):
data.la.m <- data.la[which(data.la$Sex==1), ]
data.la.f <- data.la[which(data.la$Sex==2), ]
data.la.unk <- data.la[which(data.la$Sex>=3),]

# Create 'Fleet' Column for sorting data by fleet and formatting for SS:
data.la$Fleet <- NA
data.la$Fleet[which(data.la$Zone == "PPB" &  data.la$Gear == "LL")] <- 1
data.la$Fleet[which(data.la$Zone == "PPB" &  data.la$Gear == "HS")] <- 2
data.la$Fleet[which(data.la$Season == "Spring" | data.la$Season == "Winter")] <- 99
data.la$Fleet[which(data.la$Zone == "PPB" & data.la$Gear == "RR" & data.la$Fleet == 99)] <- 4
data.la$Fleet[which(data.la$Zone == "PPB" & data.la$Gear == "RR" & data.la$Season == "Winter")] <- 5

# Remove data not correspodning to one of above fleets:
data.la <- data.la[-which(is.na(data.la$Fleet)),]
data.la <- data.la[-which(data.la$Fleet==99),]

# Create data frames for PPB Longline, Seine Fleet, Pinky, and Adult :
data.la.ppb.ll <- data.la[which(data.la$Fleet == 1),]
data.la.ppb.hs <- data.la[which(data.la$Fleet == 2),]
data.la.ppb.pny <- data.la[which(data.la$Fleet == 4),]
data.la.ppb.adt <- data.la[which(data.la$Fleet == 5),]

#Create fleet and sex specific dataframes:
data.la.ppb.ll.m <- data.la.ppb.ll[which(data.la.ppb.ll$Sex==1), ]
data.la.ppb.ll.f <- data.la.ppb.ll[which(data.la.ppb.ll$Sex==2), ]
data.la.ppb.hs.m <- data.la.ppb.hs[which(data.la.ppb.hs$Sex==1), ]
data.la.ppb.hs.f <- data.la.ppb.hs[which(data.la.ppb.hs$Sex==2), ]
data.la.ppb.pny.m <- data.la.ppb.pny[which(data.la.ppb.pny$Sex==1), ]
data.la.ppb.pny.f <- data.la.ppb.pny[which(data.la.ppb.pny$Sex==2), ]
data.la.ppb.adt.m <- data.la.ppb.adt[which(data.la.ppb.adt$Sex==1), ]
data.la.ppb.adt.f <- data.la.ppb.adt[which(data.la.ppb.adt$Sex==2), ]

#---------------------------------------------------------
# Set desired bin size and create column for length bin:
#---------------------------------------------------------

# Set desired bin size:
bin.size <- 2

# Determine vector of length bins, lower bounds (break points), given bin size:
# min.bin <- floor(min(data.la$Length) / bin.size) * bin.size
# max.bin <- floor(max(data.la$Length) / bin.size) * bin.size

# These have been set to match with the length bin vector used in SS, from the length analysis file:
min.bin <-14
max.bin <-92
bin.vector <- seq(min.bin, max.bin, bin.size)

# Add column to categorise length data into length bins (cut by bin vector):
data.la$Length.Bin <- cut(data.la$Length, breaks=bin.vector, labels=FALSE, include.lowest=TRUE)

# Get appropriate age vector from min and max ages in data:
min.age <- min(data.la$Age)
max.age <- max(data.la$Age)
age.vector <- seq(min.age, max.age, 1)

#----------------------------------------------------------------------
# Reformat flat data file into frequency table for SS: SEXES COMBINED
#----------------------------------------------------------------------

data.names <- paste("Freq", ".", age.vector, sep="")
ss.age.table <- data.frame(matrix(age.vector, 1, length(age.vector)))
names(ss.age.table) <- data.names

for(i in unique(data.la$Fleet)){
		
	# Get fleet-specific data for current loop:
	data.la.fleet 		<- data.la[which(data.la$Fleet==i), ]
	
	# Get count of samples by age and length, then reshape dataframe and order by year:
	count.table.fleet 	<- table(Year=data.la.fleet$Year, Age=data.la.fleet$Age, Length.Bin=data.la.fleet$Length.Bin) 
	frame.table.fleet  	<- as.data.frame(count.table.fleet)
	stack.frame 		<- reshape(frame.table.fleet, idvar=c("Length.Bin","Year"), timevar = "Age", direction = "wide")
	sort.stack.frame	<- stack.frame[order(stack.frame$Year), ]
		
	# Add columns for number of Samples, Sex (MF), and Fleet:
	sort.stack.frame$Samples 	<- as.vector(apply(sort.stack.frame[,-(1:2),], 1, sum))
	sort.stack.frame$Fleet 	 	<- i
	
	# Build final table by adding new table each loop:
	ss.age.table <- rbind.fill(ss.age.table, sort.stack.frame)
}

# Remove first row (not needed) and add necessary columns for SS formatted data with set values:
ss.age.table 			<- ss.age.table[-1,]
ss.age.table$Sex 		<- 0
ss.age.table$Season		<- 1
ss.age.table$Partition 	<- 2
ss.age.table$Age.Err 	<- 1
ss.age.table$Lbin.Lo 	<- ss.age.table$Length.Bin
ss.age.table$Lbin.Hi 	<- ss.age.table$Length.Bin

# If any rows have zero samples, remove those rows:
if(sum(ss.age.table$Samples[-1]==0) > 0) ss.age.table <- ss.age.table[-which(ss.age.table$Samples==0), ]

# Sort columns by name, then remove data portion of dataframe, then create extra zero dataframe of same size:
sorted.names 			<- ss.age.table[,mixedorder(names(ss.age.table))]
data.columns			<- seq(which(names(sorted.names)=="Freq.1"), which(names(sorted.names)=="Lbin.Hi")-1)
data.matrix				<- sorted.names[, data.columns]
data.matrix.extra		<- matrix(0, nrow(data.matrix), ncol(data.matrix))

# Make NA values equal to zero:
data.matrix[is.na(data.matrix)] <- 0

# Create final SS formatted table with selected columns together with data portion and extra zero data:
ss.age 		<- ss.age.table[ , c("Year", "Season", "Fleet", "Sex", "Partition", "Age.Err", "Lbin.Lo", "Lbin.Hi", "Samples")]
ss.age 		<- cbind(ss.age, data.matrix, data.matrix.extra)

# Write the data.frame to file:
write.table(ss.age, paste(out.dir,"SS_Cond_Age_Length_Table.txt",sep="/"), row.names=FALSE, quote=FALSE, sep="\t")

# Get number of age observations from dimensions of data.frame:
nage.obs <- dim(ss.age)[1] 
print(nage.obs)

# Check if length bin designations refer to the expected length bin designations from the length bin vector.
# Check consistency for length data table and use methods here to make more efficient and clean (and correct).

#---------------------------------------------------------------------------
# Reformat flat data file into frequency table for SS: SEX SPECIFIC
#---------------------------------------------------------------------------

ss.age.table.sex <- data.frame(matrix(age.vector,1,length(age.vector)))
data.names <- paste("Freq",".",age.vector,sep="")
names(ss.age.table.sex) <- data.names

for(i in unique(data.la$Fleet)){

	for(j in 1:2){
		
		# Get fleet- and sex-specific data for current loop:
		data.la.fleet 			<- data.la[which(data.la$Fleet==i), ]
		data.la.fleet.sex 		<- data.la.fleet[which(data.la.fleet$Sex==j), ]
		
		# Get count of samples by age and length, then reshape dataframe and order by year:
		count.table.fleet.sex 	<- table(Year=data.la.fleet.sex$Year, Age=data.la.fleet.sex$Age, Length.Bin=data.la.fleet.sex$Length.Bin) 
		frame.table.fleet.sex  	<- as.data.frame(count.table.fleet.sex)
		stack.frame.sex 		<- reshape(frame.table.fleet.sex, idvar=c("Length.Bin","Year"), timevar = "Age", direction = "wide")
		sort.stack.frame.sex	<- stack.frame.sex[order(stack.frame.sex$Year), ]
		
		# Add columns for number of Samples, Sex (MF), and Fleet:
		sort.stack.frame.sex$Samples 	<- as.vector(apply(sort.stack.frame.sex[,-(1:2),], 1, sum))
		sort.stack.frame.sex$Fleet 	 	<- i
		sort.stack.frame.sex$MF			<- j
		
		# Write final table by adding new table each loop:
		ss.age.table.sex <- rbind.fill(ss.age.table.sex, sort.stack.frame.sex)
		
	}	
}

# Add necessary columns for SS formatted data and set values:
ss.age.table.sex$Sex 		<- 3
ss.age.table.sex$Season		<- 1
ss.age.table.sex$Partition 	<- 2
ss.age.table.sex$Age.Err 	<- 0
ss.age.table.sex$Lbin.Lo 	<- as.numeric(ss.age.table.sex$Length.Bin)
ss.age.table.sex$Lbin.Hi 	<- as.numeric(ss.age.table.sex$Length.Bin)

# Split dataframes for males and females for rearranging into SS format:
ss.age.table.sex.f			<- ss.age.table.sex[-which(ss.age.table.sex$MF == 2), ]
ss.age.table.sex.m			<- ss.age.table.sex[-which(ss.age.table.sex$MF == 1), ]

names(ss.age.table.sex.f)[1:length(age.vector)] <- paste("Freq",".F",age.vector,sep="")
names(ss.age.table.sex.f)[which(names(ss.age.table.sex.f)=="Samples")] <- "Samples.F"

names(ss.age.table.sex.m)[1:length(age.vector)] <- paste("Freq",".M",age.vector,sep="")
names(ss.age.table.sex.m)[which(names(ss.age.table.sex.m)=="Samples")] <- "Samples.M"

# TODO: Pick up from here, above works well enough, need to work out how to merge (or join) dataframes somehow.
ss.age.table <- join(ss.age.table.sex.m, ss.age.table.sex.f, by= c("Year","Length.Bin","Fleet","MF","Sex","Season","Partition","Age.Err","Lbin.Lo","Lbin.Hi"))

# Sort columns by name, for both male and female:
sorted.names		<- ss.age.table[,mixedorder(names(ss.age.table))]

# Remove data portion male and female data.frames, then join together:
data.columns		<- seq(which(names(sorted.names)=="Freq.F1"), which(names(sorted.names)=="Lbin.Hi")-1)
data.matrix			<- sorted.names[, data.columns]

# Make NA values equal to zero:
data.matrix[is.na(data.matrix)] <- 0
#data.matrix 					<- format(round(data.matrix,3), nsmall=3) # This will allow for display of numbers with extra decimal places.

# If any rows have zero samples, remove those rows:
# TODO: Currently samples have been used to join: Need different numbers for male and female samples: then a combined samples column.
if(sum(ss.age.table$Samples[-1]==0) > 0) ss.age.table <- ss.age.table[-which(ss.age.table$Samples==0), ]


# Create final SS formatted table with selected columns together with data portion:
ss.age	<- ss.age.table[ , c("Year", "Season", "Fleet", "Sex", "Partition", "Age.Err", "Lbin.Lo", "Lbin.Hi", "Samples")]
ss.age 	<- cbind(ss.age,data.matrix)
ss.age	<- ss.age[-1,]
	

# Write the data.frame to file:
write.table(ss.age, paste(out.dir,"SS_Cond_Age_Length_Table.txt",sep="/"), row.names=FALSE, quote=FALSE, sep="\t")

# Get number of age observations from dimensions of data.frame:
nage.obs <- dim(ss.age)[1] - 1 
print(nage.obs)


#-------------------------------------------------------------------------
# Count data collected at Boat Ramps by Area, Gear, etc. and print tables
#-------------------------------------------------------------------------

area.count <- aggregate(data.la.ppb$Area,list(Area=data.la.ppb$Area),length)
names(area.count)[2] <- "Count"
area.count <- area.count[rev(order(area.count$Count)),]
row.names(area.count)=NULL
print(area.count)

last.area <- 10
area.count <- rbind(area.count[1:last.area, ], data.frame("Area"="Other","Count"=sum(area.count[(last.area+1):(nrow(area.count)),"Count"])))
print(area.count)

area.table <- xtable(area.count,caption=paste("Number of Age-Length samples of",species,"per sampling area in Port Phillip Bay for 1994-2012",sep=" "),label="table:area.count")

file.create(paste(out.dir,"/Agelen_Area_Count",data.year,".tex",sep=""),overwrite=T)
capture.output(print(area.table,include.rownames=FALSE),file=paste(out.dir,"/Agelen_Area_Count",data.year,".tex",sep=""))

#--------------------------------------
# Set parameters for growth analysis
# -------------------------------------

#Set age of interest (for particular plots);
plot.age <- 4

#Set minimum and maximum cohorts of interest, they are specific to plotting, not to calculations;
cmin <- 1990
cmax <- 2006
cgd.min <- 1992
cgd.max <- 2004

cmin <- NA
cmax <- NA
cgd.min <- NA
cgd.max <- NA

#If cohort min/max values are not specified, then get a best guesss;
if(is.na(cmin)){
	cmin=floor(mean(data.la$Cohort)-2*sd(data.la$Cohort))}
if(is.na(cmax)){
	cmax=ceiling(mean(data.la$Cohort)+2*sd(data.la$Cohort))}
	
if(is.na(cgd.min)){
	cgd.min=floor(mean(data.la$Cohort)-2*sd(data.la$Cohort))}
if(is.na(cgd.max)){
	cgd.max=ceiling(mean(data.la$Cohort)+2*sd(data.la$Cohort))}	

#Use cmin and cmax to get the number of cohorts for plotting and set plot lengths;	
cnum=length(unique(data.la$Cohort)[unique(data.la$Cohort)>=cmin&unique(data.la$Cohort)<=cmax])
cplotx=round(sqrt(cnum))
cploty=ifelse((sqrt(cnum)-floor(sqrt(cnum)))<=0.5,cplotx+1,cplotx)

#Calculate the plotting lengths and widths in terms of years;
ynum=length(unique(data.la$Year)[unique(data.la$Year)])
yplotx=round(sqrt(ynum))
yploty=ifelse((sqrt(ynum)-floor(sqrt(ynum)))<=0.5,yplotx+1,yplotx)

#Set some objects to help with plotting (maximum age and length from data set);
amax=max(data.la$Age)
lmax=max(data.la$Length)
amax.plot=max(data.la$Age)+2
lmax.plot=max(data.la$Length)+10

#Get maximum and minimum ages for males and females;
amax.m=max(data.la.m$Age)
lmax.m=max(data.la.m$Length)
amax.f=max(data.la.f$Age)
lmax.f=max(data.la.f$Length)

#Get maximum and minimum ages for males and females for LL fleet;
amax.ll.m=max(data.la.ppb.ll.m$Age)
lmax.ll.m=max(data.la.ppb.ll.m$Length)
amax.ll.f=max(data.la.ppb.ll.f$Age)
lmax.ll.f=max(data.la.ppb.ll.f$Length)

#Get maximum and minimum ages for males and females for HS fleet;
amax.hs.m=max(data.la.ppb.hs.m$Age)
lmax.hs.m=max(data.la.ppb.hs.m$Length)
amax.hs.f=max(data.la.ppb.hs.f$Age)
lmax.hs.f=max(data.la.ppb.hs.f$Length) 

#Get maximum and minimum ages for males and females for PNY fleet;
amax.pny.m=max(data.la.ppb.pny.m$Age)
lmax.pny.m=max(data.la.ppb.pny.m$Length)
amax.pny.f=max(data.la.ppb.pny.f$Age)
lmax.pny.f=max(data.la.ppb.pny.f$Length)

#Get maximum and minimum ages for males and females for ADT fleet;
amax.adt.m=max(data.la.ppb.adt.m$Age)
lmax.adt.m=max(data.la.ppb.adt.m$Length)
amax.adt.f=max(data.la.ppb.adt.f$Age)
lmax.adt.f=max(data.la.ppb.adt.f$Length)

#-------------------------------------
# Analyse growth data:
#-------------------------------------

#Fit a Von Bertalanffy curve to pooled data, then male and female data. Guess starting values suitable for most species; t0=0, Linf=Guess from LMax, k=0.2 
Li=lmax-10
Li.m=lmax.m-10
Li.f=lmax.f-10
K=0.2
t0=0

#Set VonBert Equation;
VonBert=Length~Li*(1-exp(-K*(Age-t0)))

#Fit and predict for VonBert to all data;
vb.model=nls(VonBert,start=list(Li=Li,K=K,t0=t0),data=data.la)
summary(vb.model)
xv=seq(1,amax,0.1)
yv=predict(vb.model,list(Age=xv))

#Fit and predict for VonBert to Males;
vb.model.m <- nls(VonBert, start=list(Li=Li,K=K,t0=t0), data=data.la.m)
summary(vb.model.m)
xv.m=seq(1,amax.m,0.1)
yv.m=predict(vb.model.m,list(Age=xv.m))

#Fit and predict for VonBert to Females;
vb.model.f <- nls(VonBert, start=list(Li=Li,K=K,t0=t0), data=data.la.f)
summary(vb.model.f)
xv.f=seq(1,amax.f,0.1)
yv.f=predict(vb.model.f,list(Age=xv.f))

#Fit and predict for VonBert to males for LL fleet:
vb.model.ll.m <- nls(VonBert, start=list(Li=Li,K=K,t0=t0), data=data.la.ppb.ll.m)
summary(vb.model.ll.m)
xv.ll.m=seq(1,amax.ll.m,0.1)
yv.ll.m=predict(vb.model.ll.m,list(Age=xv.ll.m))

#Fit and predict for VonBert to females for LL fleet:
vb.model.ll.f <- nls(VonBert, start=list(Li=Li,K=K,t0=t0), data=data.la.ppb.ll.f)
summary(vb.model.ll.f)
xv.ll.f=seq(1,amax.ll.f,0.1)
yv.ll.f=predict(vb.model.ll.f,list(Age=xv.ll.f))

#Fit and predict for VonBert to males for HS fleet:
vb.model.hs.m <- nls(VonBert, start=list(Li=Li.m,K=K,t0=t0), data=data.la.ppb.hs.m)
summary(vb.model.hs.m)
xv.hs.m=seq(1,amax.hs.m,0.1)
yv.hs.m=predict(vb.model.hs.m,list(Age=xv.hs.m))

#Fit and predict for VonBert to females for HS fleet (not enough data to work):
vb.model.hs.f <- nls(VonBert, start=list(Li=Li.f,K=K,t0=t0), data=data.la.ppb.hs.f)
summary(vb.model.hs.f)
xv.hs.f=seq(1,amax.hs.f,0.1)
yv.hs.f=predict(vb.model.hs.f,list(Age=xv.hs.f))

#Fit and predict for VonBert to males for PNY fleet:
vb.model.pny.m <- nls(VonBert, start=list(Li=Li.m,K=K,t0=t0), data=data.la.ppb.pny.m)
summary(vb.model.pny.m)
xv.pny.m=seq(1,amax.pny.m,0.1)
yv.pny.m=predict(vb.model.pny.m,list(Age=xv.pny.m))

#Fit and predict for VonBert to females for PNY fleet:
vb.model.pny.f <- nls(VonBert, start=list(Li=Li.f,K=K,t0=t0), data=data.la.ppb.pny.f)
summary(vb.model.pny.f)
xv.pny.f=seq(1,amax.pny.f,0.1)
yv.pny.f=predict(vb.model.pny.f,list(Age=xv.pny.f))

#Fit and predict for VonBert to males for ADT fleet:
vb.model.adt.m <- nls(VonBert, start=list(Li=Li.m,K=K,t0=t0), data=data.la.ppb.adt.m)
summary(vb.model.adt.m)
xv.adt.m=seq(1,amax.adt.m,0.1)
yv.adt.m=predict(vb.model.adt.m,list(Age=xv.adt.m))

#Fit and predict for VonBert to females for ADT fleet:
vb.model.adt.f <- nls(VonBert, start=list(Li=Li.f,K=K,t0=t0), data=data.la.ppb.adt.f)
summary(vb.model.adt.f)
xv.adt.f=seq(1,amax.adt.f,0.1)
yv.adt.f=predict(vb.model.adt.f,list(Age=xv.adt.f))

#Get unique points for benefit of plotting;
data.la.plot=unique(data.la[,c("Length","Age")])
data.la.plot.m=unique(data.la.m[,c("Length","Age")])
data.la.plot.f=unique(data.la.f[,c("Length","Age")])
data.la.plot.ll.m=unique(data.la.ppb.ll.m[,c("Length","Age")])
data.la.plot.ll.f=unique(data.la.ppb.ll.f[,c("Length","Age")])
data.la.plot.hs.m=unique(data.la.ppb.hs.m[,c("Length","Age")])
data.la.plot.hs.f=unique(data.la.ppb.hs.f[,c("Length","Age")])
data.la.plot.pny.m=unique(data.la.ppb.pny.m[,c("Length","Age")])
data.la.plot.pny.f=unique(data.la.ppb.pny.f[,c("Length","Age")])
data.la.plot.adt.m=unique(data.la.ppb.adt.m[,c("Length","Age")])
data.la.plot.adt.f=unique(data.la.ppb.adt.f[,c("Length","Age")])

#-------------------------------------
# Plot Growth Data and Model Fits:
#-------------------------------------

# Basic Growth Analysis. Plot observed and expected growth, and compare growth 
# curves between males and females and also with comparison species if required.

#Plot all growth pooled;
plot(data.la.plot$Age,data.la.plot$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",main="Observed & Predicted Length at Age for Snapper")
lines(xv,yv,col="black",lty=1,lwd=3)
  
#Save growth pooled plot to output folder in PNG format;
png(paste(out.dir,"/","Observed_and_Predicted_Length_at_Age.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
plot(data.la.plot$Age,data.la.plot$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)")
lines(xv,yv,col="black",lty=1,lwd=3)
dev.off()

#Plot male growth per fleet:
plot(data.la.plot.ll.m$Age,data.la.plot.ll.m$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",main="Observed Length at Age for Male Snapper",abline(h = 0, col = "grey"))
points(data.la.plot.hs.m$Age,data.la.plot.hs.m$Length,col="grey")
points(data.la.plot.pny.m$Age,data.la.plot.pny.m$Length,col="grey")
points(data.la.plot.adt.m$Age,data.la.plot.adt.m$Length,col="grey")
lines(xv.ll.m,yv.ll.m,col="Dark Blue",lty=1,lwd=3)
lines(xv.hs.m,yv.hs.m,col="Dark Green",lty=1,lwd=3)
lines(xv.pny.m,yv.pny.m,col="Red",lty=1,lwd=3)
lines(xv.adt.m,yv.adt.m,col="Purple",lty=1,lwd=3)
legend("topleft", bty = "n", c("LL", "HS", "PNY", "ADT"),lty = 1,lwd=2, col = c("Dark Blue", "Dark Green", "Red", "Purple"))

#Save male growth plot to output folder in PNG format;
png(paste(out.dir,"/","Male_Length_at_Age_Per_Fleet.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
plot(data.la.plot.ll.m$Age,data.la.plot.ll.m$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",abline(h = 0, col = "grey"))
points(data.la.plot.hs.m$Age,data.la.plot.hs.m$Length,col="grey")
points(data.la.plot.pny.m$Age,data.la.plot.pny.m$Length,col="grey")
points(data.la.plot.adt.m$Age,data.la.plot.adt.m$Length,col="grey")
lines(xv.ll.m,yv.ll.m,col="Dark Blue",lty=1,lwd=3)
lines(xv.hs.m,yv.hs.m,col="Dark Green",lty=1,lwd=3)
lines(xv.pny.m,yv.pny.m,col="Red",lty=1,lwd=3)
lines(xv.adt.m,yv.adt.m,col="Purple",lty=1,lwd=3)
legend("topleft", bty = "n", c("LL", "HS", "PNY", "ADT"),lty = 1,lwd=2, col = c("Dark Blue", "Dark Green", "Red", "Purple"))
dev.off()

#Plot female growth per fleet:
plot(data.la.plot.ll.f$Age,data.la.plot.ll.f$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",main="Observed Length at Age for Female Snapper",abline(h = 0, col = "grey"))
points(data.la.plot.hs.f$Age,data.la.plot.hs.f$Length,col="grey")
points(data.la.plot.pny.f$Age,data.la.plot.pny.f$Length,col="grey")
points(data.la.plot.adt.f$Age,data.la.plot.adt.f$Length,col="grey")
lines(xv.ll.f,yv.ll.f,col="Dark Blue",lty=1,lwd=3)
lines(xv.pny.f,yv.pny.f,col="Red",lty=1,lwd=3)
lines(xv.adt.f,yv.adt.f,col="Purple",lty=1,lwd=3)
legend("topleft", bty = "n", c("LL", "PNY", "ADT"),lty = 1,lwd=2, col = c("Dark Blue", "Red", "Purple"))

#Save female growth plot to output folder in PNG format;
png(paste(out.dir,"/","Female_Length_at_Age_Per_Fleet.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
plot(data.la.plot.ll.f$Age,data.la.plot.ll.f$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)", abline(h = 0, col = "grey"))
points(data.la.plot.hs.f$Age,data.la.plot.hs.f$Length,col="grey")
points(data.la.plot.pny.f$Age,data.la.plot.pny.f$Length,col="grey")
points(data.la.plot.adt.f$Age,data.la.plot.adt.f$Length,col="grey")
lines(xv.ll.f,yv.ll.f,col="Dark Blue",lty=1,lwd=3)
lines(xv.pny.f,yv.pny.f,col="Red",lty=1,lwd=3)
lines(xv.adt.f,yv.adt.f,col="Purple",lty=1,lwd=3)
legend("topleft", bty = "n", c("LL", "PNY", "ADT"),lty = 1,lwd=2, col = c("Dark Blue", "Red", "Purple"))
dev.off()

#Plot male and female growth separately;
plot(data.la.plot.m$Age,data.la.plot.m$Length,pch=1,col="light blue",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",main="Observed Length at Age for Snapper",abline(h = 0, col = "grey"))
grid()
points(data.la.plot.f$Age,data.la.plot.f$Length,col="light pink")
lines(xv.f,yv.f,col="Red",lty=1,lwd=3)
lines(xv.m,yv.m,col="Dark Blue",lty=1,lwd=3)
legend("topleft", bty = "n", c("Females", "Males"),lty = 1,lwd=2, col = c("Red", "Dark Blue"))

#Save male and female growth plot to output folder in PNG format;
png(paste(out.dir,"/","Male_and_Female_Length_at_Age.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
plot(data.la.plot.m$Age,data.la.plot.m$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",abline(h = 0, col = "grey"))
grid()
points(data.la.plot.f$Age,data.la.plot.f$Length,col="grey")
lines(xv.f,yv.f,col="Red",lty=1,lwd=3)
lines(xv.m,yv.m,col="Dark Blue",lty=1,lwd=3)
legend("topleft", bty = "n", c("Females", "Males"),lty = 1,lwd=2, col = c("Red", "Dark Blue"))
dev.off()

#Compare mean length-at-age data with VonBert model on a plot;
plot(data.la.plot$Age,data.la.plot$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",main="Mean vs. VonBert Growth for Snapper")
lines(xv,yv,col="black",lty=1,lwd=2)
MLA=aggregate(data.la$Length,list(Age=data.la$Age),mean)
names(MLA)=c("Age","Mean.Length")
points(MLA,col="Red",pch=18,type="l",lwd=2)

#Save mean length-at-age data with VonBert plot to output folder in PNG format;
png(paste(out.dir,"/","Meangrowth_vs_VonBert_model.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
plot(data.la.plot$Age,data.la.plot$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",)
lines(xv,yv,col="black",lty=1,lwd=2)
MLA=aggregate(data.la$Length,list(Age=data.la$Age),mean)
names(MLA)=c("Age","Mean.Length")
points(MLA,col="Red",pch=18,type="l",lwd=2)
dev.off()



