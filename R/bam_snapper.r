#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#
# *bam.r : Basic Assessment Model script for Fisheries Victoria
# This script implements a simple population model to determine common 
# biological reference points. Option allows for a comparison of reference
# points when a stock has different growth trajectories (variable K, vonBert growth).
#
# Script can loop over multiple selected species: enter multiple numbers
# in species list e.g. species.list <- c(1:3,5,8)
#
# by Athol Whitten, November 2012.
# athol.whitten@mezo.com.au
#
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#Remove previous R console object assignments:
rm(list=ls())

#Record start time (to aid loop control and timing of large model runs):
start.time <- Sys.time()

#Set root directory and working folders:
working.folder <- "C:/Dropbox/Mezo/Projects/Fishvic/"
data.dir <- paste(working.folder,"/Data/",sep="")

#Source required functions:
source(paste(working.folder,"/Scripts/fishfunc.r",sep=""))
  
#Read in the Victorian Fisheries Parameters File:
datafile <- read.csv(paste(data.dir,"FV_Parms_2012.csv",sep=""),header=TRUE)

#Format of output: to screen only, or additionally as  .png and .txt files to folder? (Set "Screen" or "PNG"):
output.format <- "PNG"

#Set folder for outputs if required:
output.folder <- paste(working.folder,"/Output/",sep="")
dir.create(output.folder)

#Species list:
# 1	 Pink Snapper
# 2	 Black Bream

species.list <- c(1,2)
#Set species of interest:
species.output  <-  NULL

for(species in species.list){
	species.name <- datafile$Species[species]

	#Set the level of deviation (multiplier to K) for analyses:
	min.dev <- 0.75
	max.dev <- 1.25				
	inc.dev <- 0.05   #Increment the growth deviation scalar by this amount.

	#Create a list of Deviation Values to loop over:
	gdev.lims <- seq(min.dev,max.dev,inc.dev)

	#Set FALSE for Dev test if requiring straight forward analyses with no growth-deviation checks:
	dev.test <- FALSE

	#Plot all reference points as part of Dev test? Or just Fmsy, Bmsy? (Set plot.all <- TRUE for all plots, FALSE for Fmsy and Bmsy):
	plot.all <- FALSE

	if(dev.test==FALSE){gdev.lims=1}

	#Set logistic or knife-edge maturity ("KE" or "Logistic"):
	mat.type <- "KE"

	#Get species specific parameters from input file:
	stp <- datafile$h[species]			#Steepness, BH Stock-Recruitment Curve
	amax <- datafile$amax[species]		#Maximum age (for plus group)
	M <- datafile$M[species]			#Natural Mortality
	Li <- datafile$Linf[species]		#Growth (VonBert Length Infinity)
	k <- datafile$k[species]			#Growth (VonBert Growth Rate Coeffient)
	t0 <- datafile$t0[species]			#Growth (VonBert Length at Time Zero)
	a <- datafile$a[species]			#Weight at Length Relationship (a)
	b <- datafile$b[species]			#Weight at Length Relationship (b)
	LMat1 <- datafile$lmat[species]		#Length at 50% Maturity (Can be used for Knife-Edge of Logistic Maturity)
	LMat2 <- datafile$lmat2[species]	#Slope parameter for Logistic Maturity 

	#Get the length selectivity parameters (logistic relationship):
	LS25 <- datafile$l25[species]		#Length at 25% Selectivity
	LS50 <- datafile$l50[species]		#Length at 50% Selectivity 

	#Calculate Length at 95%-Selectivity given values for 50% and 25% selectivity:
	LS95 <- LSel95(LSel25=LS25,LSel50=LS50)

	#Create vector of species parameters:
	species.parms <- round(c("M"=M,"h"=stp,"Linf"=Li,"k"=k,"t0"=t0,"a"=a*1000,"b"=b,"L25"=LS25,"L50"=LS50,"LMat1"=LMat1,"LMat2"=LMat2,"amax"=amax),4)


	#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# Reference point calculation section:

	#Create tables to summarise results and parameters-at-age used (over set of growth deviation factors):
	MSYPR <- data.frame()
	dev.age.matrix <- data.frame()

	#Create a data frame to record all subsequent YPR calculations (for each Dev value):
	YPR.Dev <- data.frame()
	for(Dev in gdev.lims){									#Loop all YPR calculations over the Growth Deviation Factor

		#Yield-per-recruit analyses:
		#Create a matrix of selectivity-, length-, weight- and maturity-at-age:
		age.matrix <- data.frame()

		for(i in 0:amax){
			
			age <- i+0.5											#Set age to mean of age group (+0.5)		
			L <- VBDevK(age,Li,k,t0,Dev)							
			if(L<0.1){L <- 0.1}										#Get length-at-age (include trap so that length must be at least 0.1 cm, to replicate SESSF T3 anlaysis)
			W <- WLen(L,a,b)										#Get weight at current length	
			S <- 1/(1+(exp(-log(19)*((L-LS50)/(LS95-LS50)))))		#Get selectivity at current length
			
			if(mat.type=="KE"){ 
				ifelse(L > LMat1, Mat <- 1.0, Mat <- 0.0)			#Get maturity at length (knife-edge: value of 0 or 1)
			}					
			
			if(mat.type=="Logistic"){
				Mat <- 1/(1+exp(LMat2*(L-LMat1)))					#Get maturity at length (logistic curve: value between 0 and 1)
			}						
			
			next.row <- cbind(Dev=Dev,Age=i,Length=L,Weight=W,NatMort=M,Select=S,Maturity=Mat,Spawning_Bio=W*Mat)	
			age.matrix <- rbind(age.matrix,next.row)
		
		}

		dev.age.matrix <- rbind(dev.age.matrix,age.matrix)

		#Develop yield-per-recruit summary for multiple F values:
		Fmin <- 0.0000
		Fmax <- 1.0000
		Finc <- 0.001

		fmort <- seq(Fmin,Fmax,Finc)	#Get sequence of fishing mortality values
		Nr <- 1							#Set initial number of recruits to one
	  
		VS <- data.frame(Age=0,Number=Nr,Biomass=age.matrix[1,"Weight"]*Nr,Spawning_Bio=age.matrix[1,"Spawning_Bio"])	#Begin data frame for Virgin Stock (VS), (Age zero with (Nr) recruits, and biomass of weight at age 0 (mean age = 0.5), spawning biomass = 0):
		B <- 0
		Sp <- 0
		
		for(j in 1:(amax)){															#For ages 1 through to maximum age
			Np <- VS[j,"Number"]*(exp(-M))											#Get number expected at age j (from age j-1)
			if(j==amax){															#Make adjustment for plus group
			Np <- VS[j,"Number"]*(exp(-M))/(1-(exp(-M)))}
			B <- (age.matrix[j+1,"Weight"])*Np											#Get Biomass at age
			Sp <- ((age.matrix[j+1,"Weight"])*Np*(age.matrix[j+1,"Maturity"]))/2		#Get Spawning biomass at age (divide by 2 as only for females)
			next.row <- c(j,Np,B,Sp)													#Create row for current age								
			VS <- rbind(VS,next.row)													#Add row to Virgin Stock dataframe.
		}                  

		B <- sum(VS$B)									#Calculate virgin total biomass
		B0 <- sum(VS$Sp)								#Calculate virgin spawning biomass
		alpha <- ((B0*(1-stp))/(4*stp*Nr))				#Calculate alpha for BH Stock Recruitment
		beta <- (((5*stp)-1)/(4*stp*Nr))				#Calculate beta for BH Stock Recruitment			
		R0 <- B0/(alpha+(beta*B0))						#Calculate virgin recruitment (just as a check, should be equal to  Nr)
		RSpR <- (B0/B0)*R0								#Calculate virgin relative spawning biomass times recruit (another check, should be = to 1).
	 
	  
		YPR <- data.frame(Dev,0,R0,B,B0,0,0,RSpR)		#Begin data frame for Yield-per-Recruit calculations, (VS) values become first row:
		names(YPR) <- c("Dev","Fmort","Recruitment","Biomass","Spawning.Biomass","Yield.Recruit","Total.Yield","Relative.Sp.R") 

		FYPR <- data.frame()

		for(f in fmort){								#Loop YPR calculations over set fishing mortality range:	
	
			N <- data.frame()

			#data.frame(Age=0,Number=Nr,Biomass=age.matrix[1,"Weight"],Spawning_Bio=age.matrix[1,"Spawning_Bio"],Yield=0)	#Begin data frame for numbers, biomass, spawning biomass and yield at age:

			Y <- 0																			#Reset Y, Sp and B to zero for start of each loop	
			Sp <- 0
			B <- 0

			for(j in 1:(amax+1)){														#For ages 0 though to maximum age
				S <- age.matrix[j,"Select"]
				Z <- age.matrix[j,"NatMort"]+(age.matrix[j,"Select"]*f)					#Get total mortality at age = j-1;			
			
				if(j==1){
					Np <- Nr
				}
				
				if(j>1){
					Zm <- age.matrix[j-1,"NatMort"]+(age.matrix[j-1,"Select"]*f)
					Np <- N[j-1,"Number"]*(exp(-Zm))
				}																		#Calculate numbers at age given total mortality rates at age j-1;
				
				if(j==(amax+1)){														#Make adjustment for plus group;
					Np <- N[j-1,"Number"]*(exp(-Zm))/(1-(exp(-Z)))						#Calculate numbers in plus group;						
				}																		
			
				B <- (age.matrix[j,"Weight"])*Np											#Get biomass at age;
				Sp <- ((age.matrix[j,"Weight"])*Np*(age.matrix[j,"Maturity"]))/2			#Get spawning biomass at age (devide by 2 to get female only spawning biomass);
				Y <- (age.matrix[j,"Weight"])*((S*f)/Z)*Np*(1-(exp(-Z)))					#Calculate yield at age;
				next.row <- (c(j-1,Np,B,Sp,Y))												#Create row of values for current age;		
				N <- rbind(N,next.row)														#Add row to Numbers data frame.
				names(N) <- c("Age","Number","Biomass","Spawning.Biomass","Yield")
			}

		  B <- sum(N$Biomass)						#Get sum for total biomass at this F
		  Sp <- sum(N$Spawning.Biomass)				#Get sum for spawning biomass at this F
		  R <- Sp/(alpha+(beta*Sp))					#Calculate recruitment at this F
		  RSpR <- (Sp/B0)*R							#Calculate relative spawning biomass (times recruitment) at this F
		  Y <- sum(N$Yield)							#Calculate yield (per recruit) at this F
		  TY <- Y*R									#Calculate total yield (yield x recruitment) at this F
		  next.row <- c(Dev,f,R,B,Sp,Y,TY,RSpR)		#Create YPR row for current F
		  YPR <- rbind(YPR,next.row)				#Add row to YPR data frame
		  FYPR.N <- cbind('FMort'=f,N)				#Create and record age structure matrix for this F
		  FYPR <- rbind(FYPR,FYPR.N)
		
		}

		YPR.Dev <- rbind(YPR.Dev,YPR)								#Add all YPR calculations to larger table of YPR at different growth deviations (to record and for use if required):

		max.yield <- max(YPR$Yield.Recruit)															#Get maximum yield per recruit:
		max.total.yield <- max(YPR$Total.Yield)														#Get maximum total yield:
		max.yield.f <- YPR$Fmort[which(YPR$Yield.Recruit==max(YPR$Yield.Recruit))]					#Get F corresponding to maximum yield-per-recruit (Fmax):
		max.tyield.f <- YPR$Fmort[which(YPR$Total.Yield==max(YPR$Total.Yield))]						#Get F corresponding to maximum total yield (Fmsy):

		SpB.max.tyield.f <- YPR$Spawning.Biomass[which(YPR$Total.Yield==max(YPR$Total.Yield))]/B0	#Get Spawning Biomass at Fmsy relative to Virgin Spawning Biomass (B0) (Bmsy):

		Bmsy <- YPR$Relative.Sp.R[which(YPR$Total.Yield==max(YPR$Total.Yield))]						#Get Bmsy (Relative Spawning Biomass x Recruit at Maximum Sustainable Yield):

		FSpR20 <- mean(YPR$Fmort[which(round(YPR$Relative.Sp.R,2)==0.20)])							#Get F corresponding to 20% spawning biomass per recruit:
		FSpR40 <- mean(YPR$Fmort[which(round(YPR$Relative.Sp.R,2)==0.40)])							#Get F corresponding to 40% spawning biomass per recruit:
		FSpR48 <- mean(YPR$Fmort[which(round(YPR$Relative.Sp.R,2)==0.48)])							#Get F corresponding to 48% spawning biomass per recruit:

		if(Dev==1){FLim <- FSpR20}

		#Update results table:
		next.row <- c(Dev,max.yield,max.yield.f,FSpR20,FSpR40,FSpR48,max.tyield.f,Bmsy)	#Create row to report reference points for current growth deviation factor:
		MSYPR <- rbind(MSYPR,round(next.row,3))											#Add row to summary reporting data frame:

		names(MSYPR) <- c("Growth.Deviation","MaxYPR","Fmax","FSpR20","FSpR40","FSpR48","FMSY","BMSY")
	
	}

	output <- list(species.parms,MSYPR,dev.age.matrix,FYPR,YPR.Dev)
	names(output) <- c(paste(species.name,"_Parameters",sep=""),paste(species.name,"_Ref_Points",sep=""),paste(species.name,"_Age_Matrix",sep=""),paste(species.name,"_Numbers_F",sep=""),paste(species.name,"_YPR",sep=""))

	sink(paste(output.folder,"/",species.name,"_Ref_Point_Values.txt",sep=""))
	print(output)
	sink()

	species.output=list(species.output,output)

	#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# Plots section:

	#Colours for 4 x 4 plots:
	C1 <- "Blue"
	C2 <- "Orange"
	C3 <- "Gold"
	C4 <- "Red"

	#Call plot settings appropriate for output required:
	if(output.format=="PNG"){

		if(dev.test==TRUE){
		png(paste(output.folder,"/",species.name,"_Ref_Points_Var_Growth.png",sep=""),width=25,height=25,res=300,units="cm",pointsize=14,bg="White")}
		
		if(dev.test==FALSE){
		png(paste(output.folder,"/",species.name,"_Ref_Points.png",sep=""),width=25,height=25,res=300,units="cm",pointsize=14,bg="White")}
	}

	if(output.format=="Screen"){windows(record=TRUE)}
	par(mfrow=c(2,2))

	#Call plot with attributes for YPR vs Fishing Mortality:
	plot("","",xlab="Instantaneous Fishing Mortality (F)",ylab="Yield (kg)",xlim=c(0,max(fmort)),ylim=c(min(0),max(YPR$Yield)*1.1),main=species.name,cex.main=0.9)
	if(dev.test==FALSE){mtext(paste("Fmax = ",max.yield.f,sep=""),side=3,line=0.4,cex=0.8)}

	#Plot a line for each Dev specific YPR set:
	for(Dev in gdev.lims){
		lines(YPR.Dev$Fmort[YPR.Dev$Dev==Dev],YPR.Dev$Yield.Recruit[YPR.Dev$Dev==Dev],lwd=2,typ="l",col=C1)
		arrows(max.yield.f,0,max.yield.f,max.yield,code=0,lty=3)
		arrows(0,max.yield,max.yield.f,max.yield,code=0,lty=3)
	}

	#Call plot with attributes for Recruitment vs Fishing Mortality:
	plot("","",xlab="Instantaneous Fishing Mortality (F)",ylab="Recruitment",xlim=c(0,max(fmort)),ylim=c(min(0),max(YPR$Recruitment)*1.2))

	#Plot a line for each Dev specific YPR set:
	for(Dev in gdev.lims){  
		lines(YPR.Dev$Fmort[YPR.Dev$Dev==Dev],YPR.Dev$Recruitment[YPR.Dev$Dev==Dev],lwd=2,typ="l",col=C2)
	}

	#Call plot with attributes for (Relative Spawning Biomass x Recruitment) vs Fishing Mortality
		plot("","",xlab="Instantaneous Fishing Mortality (F)",ylab="Female Sp. Biomass x R",xlim=c(0,max(fmort)),ylim=c(min(0),max((YPR$Spawning.Biomass/B0)*YPR$Recruitment)*1.2))
		if(dev.test==FALSE){	
		mtext(c(paste("Fspr20 = ",round(FSpR20,3),"  &  Fspr48 = ",round(FSpR48,3),sep="")),side=3,line=1,cex=0.8)
	}

	#Plot a line for each Dev specific YPR set:
	for(Dev in gdev.lims){
		lines(YPR.Dev$Fmort[YPR.Dev$Dev==Dev],YPR.Dev$Relative.Sp.R[YPR.Dev$Dev==Dev],lwd=2,typ="l",col=C3)
		arrows(0,0.20,FSpR20,0.20,code=0,lty=3)
		arrows(FSpR20,0.20,FSpR20,0,code=0,lty=3)
		arrows(0,0.48,FSpR48,0.48,code=0,lty=3)
		arrows(FSpR48,0.48,FSpR48,0,code=0,lty=3)
	}

	#Call a plot with attributes for Total Yield vs Fishing Mortality:
	plot("","",xlab="Instantaneous Fishing Mortality (F)",ylab="Total Yield (Yield x R)",xlim=c(0,max(fmort)),ylim=c(min(0),max(YPR$Total.Yield)*1.1))
	if(dev.test==FALSE){	
			mtext(c(paste("Fmsy = ",round(max.tyield.f,3),"  &  Bmsy = ",round(Bmsy,3),sep="")),side=3,line=1,cex=0.8)
		}
		
		#Plot a line for each Dev specific YPR set:
		for(Dev in gdev.lims){ 
		lines(YPR.Dev$Fmort[YPR.Dev$Dev==Dev],YPR.Dev$Total.Yield[YPR.Dev$Dev==Dev],lwd=2,typ="l",col=C4)
		arrows(max.tyield.f,0,max.tyield.f,max.total.yield,code=0,lty=3)
		arrows(0,max.total.yield,max.tyield.f,max.total.yield,code=0,lty=3)
	}

	if(output.format=="PNG"){dev.off()}

	#Plot calculated reference point values against growth deviation applied to K if growth-devs are being tested:
	if(dev.test==TRUE){

		if(output.format=="PNG"){
			
			if(plot.all==FALSE){
			png(paste(output.folder,"/",species.name,"_RefPointValues_vs_GrowthDev.png",sep=""),width=24,height=12,res=300,units="cm",pointsize=14,bg="White")
			par(mfrow=c(1,2))
			}
		
			if(plot.all==TRUE){	
			png(paste(output.folder,"/",species.name,"_RefPointValues_vs_GrowthDev.png",sep=""),width=25,height=25,res=300,units="cm",pointsize=14,bg="White")
			par(mfrow=c(2,2))
			}
		}
		
		if(plot.all==TRUE){
		
			plot(MSYPR$Growth.Deviation,MSYPR$FSpR20,type="b",xlab="Multiplier on K",ylab="Fspr20",col=C1)
			abline(h=FLim,v=1,lty=2)
			plot(MSYPR$Growth.Deviation,MSYPR$FSpR40,type="b",xlab="Multiplier on K",ylab="Fspr40",col=C2)
		
		}

		plot(MSYPR$Growth.Deviation,MSYPR$FMSY,type="b",xlab="Multiplier on K",ylab="Fmsy",col=C3)
		plot(MSYPR$Growth.Deviation,MSYPR$BMSY,type="b",xlab="Multiplier on K",ylab="Bmsy",col=C4)

		if(dev.test==TRUE){dev.off()}

	}

	################## OUTPUT PRINTING AND SAVING ########################

	print(paste("Reference point table for ",species.name,sep=""))
	print(MSYPR)
	sink(paste(output.folder,"/All_Species_Ref_Point_Values.txt",sep=""))
	print(species.output)
	sink()

} #End of species list loop.

finish.time <- Sys.time()
print(start.time)
print(finish.time)
print(round(finish.time-start.time,2))

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# End of script. 

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# This code/program has been developed by Mezo Research, Melbourne, Australia, 2012/13.
#
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation (http://www.fsf.org/).
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++