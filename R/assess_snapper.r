#--------------------------------------------------------------------------------------
#
# *assess_snapper.r reformats, summarises, analyses, and plots
# Pink Snapper data from Fisheries Victoria for pre-assessment analyses.
#
# Output files are sent to the ~/Snapper/Output directory.
#
# By Athol R. Whitten and Simone R. Stuckey, Mezo Research Pty. Ltd.
# Contact: athol.whitten@mezo.com.au, Last Updated: October 2013
#
#--------------------------------------------------------------------------------------

#Remove previous R console object assignments and load required libraries;
rm(list=ls())
library(plyr)
library(xtable)

#-------------------------------------
# Set and load directories/files:
#-------------------------------------

#Set root directory:
rootdir <- "C:/Dropbox"
working.folder <- "C:/Dropbox/Fisheries/DEPI/Assessment"

#Set directory to relevant files:
species <- "Snapper"
assess.year <- 2013
data.year <- 2013
user.name <- "Athol Whitten"
data.dir <- paste(working.folder, assess.year, species, "Data", sep="/")
out.dir <- paste(working.folder, assess.year, species, "Output", sep="/")

la.data <- "/MZ_AgeLen_Snapper.csv"
la.data.batch <- "/MZ_AgeLen_Snapper_Batch_Record.csv"
len.data.rec <- "/MZ_Length_Recreational_Snapper.csv"

#Source some functions for plot diagnotics:
source(paste(rootdir, "/Modelling/R/Source/FishFunc.r",sep=""))

#Read Data (.csv) Files:
data.la.raw <- read.csv(paste(data.dir, la.data, sep=""), header=TRUE, comment.char="")
data.la.batch.raw <- read.csv(paste(data.dir, la.data.batch, sep=""), header=TRUE, comment.char="")
data.le.rec.raw <- read.csv(paste(data.dir, len.data.rec, sep=""), header=TRUE, comment.char="")

#-----------------------------------------------
# Reformat and configure data for analyses:
#-----------------------------------------------

#Create new data frames;
data.la <- data.la.raw
data.la.batch <- data.la.batch.raw
data.le.rec <- data.le.rec.raw

#Merge data.la with other information (inc. gear) from length/age batch record data file:
data.la <- join(data.la, data.la.batch, by = c("Code","Date","Zone","Area","Batch"))

#Add columns for 'Year' and 'Month', by extracting information from 'Date', as numeric values;
data.la <- cbind(data.la, Year=as.numeric(format(as.Date(data.la$Date), "%Y")))
data.la <- cbind(data.la, Month=as.numeric(format(as.Date(data.la$Date), "%m")))

# Add columns for Cohort, Cohort Name and Sample Year Name:
data.la <- cbind(data.la, Cohort=as.numeric(data.la$Year - data.la$Age))
data.la <- cbind(data.la, Cohort.Name=paste("YC",data.la$Cohort), Sample.Year=paste("Year",data.la$Year))

#Remove rows where length is zero on NA (assumed these values have been recorded in error):
data.la <- data.la[data.la$Length>0, ]
data.la <- data.la[-which(is.na(data.la$Length)), ]

#Create data frames for fish captured from Port Phillip Bay:
data.la.ppb <- data.la[data.la$Zone=="PPB", ]
data.le.rec.ppb <- data.le.rec[data.le.rec$Zone=="PPB",]

#Create dataframes for data from Port Phillip Bay, caught with Long Lines (LL) and with Rod and Reel (RR):
data.la.ppb.ll <- data.la[(data.la$Zone=="PPB") & (data.la$Gear =="LL"), ]
data.la.ppb.rr <- data.la[(data.la$Zone=="PPB") & (data.la$Gear =="RR"), ]

#-----------------------------------------------------
# Count data by Area, Gear, etc. and Capture Tables
#-----------------------------------------------------

area.count <- aggregate(data.lenr.ppb$Area,list(Area=data.lenr.ppb$Area),length)
names(area.count)[2] <- "Count"
area.count <- area.count[rev(order(area.count$Count)),]
row.names(area.count)=NULL
print(area.count)

last.area <- 16
area.count <- rbind(area.count[1:last.area, ], data.frame("Area"="Other","Count"=sum(area.count[(last.area+1):(nrow(area.count)),"Count"])))
print(area.count)

area.table <- xtable(area.count,caption=paste("Number of length samples of",species,"per ramp sampling area for 2002-2011",sep=" "),label="table:area.count")

file.create(paste(out.dir,"/Area_Count_Rec",data.year,".tex",sep=""),overwrite=T)
capture.output(print(area.table,include.rownames=FALSE),file=paste(out.dir,"/Area_Count_Rec",data.year,".tex",sep=""))


#-------------------------------------------------
# Plot length and age frequency distributions
#-------------------------------------------------

#Make a plot of the length frequency distribution of snapper caught by rod and reel from Port Phillip Bay (length data only)
hist.le.rec.ppb <- hist(data.le.rec.ppb$Fork.Length, main=NULL, xlab="Fork Length (cm)", ylab="Number", cex.lab=1.2, breaks=20, col="Light Green")

#Save plot to output folder in PNG format
png(paste(out.dir,"/","Length_Freq_Dist_Rec_PPB.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
hist.lenr.ppb <- hist(data.lenr.ppb$Fork.Length, main=NULL, xlab="Fork Length (cm)", ylab="Number", cex.lab=1.2, breaks=20, col="Light Green")
dev.off()

#Plot age-frequency distribution of snapper caught by rod and reel from Port Phillip Bay (age-length data):
hist.la.ppb.rr <- hist(data.la.ppb.rr$Age, main=NULL, xlab="Age (Yrs)", ylab="Number", cex.lab=1.2, breaks=20, col="Orange")

#Save plot to output folder in PNG format
png(paste(out.dir,"/","Age_Freq_Dist_PPB_RR.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
hist.la.ppb.rr <- hist(data.la.ppb.rr$Age, main=NULL, xlab="Age (Yrs)", ylab="Number", cex.lab=1.2, breaks=20, col="Orange")
dev.off()


#Make a plot of the season distribution:
hist.lenr.ppb <- hist(data.lenr.ppb$Month, main=NULL, xlab="Month", ylab="Number", cex.lab=1.2, breaks=12)

#Make a plot of the age-frequency distribution of snapper from Port Phillip Bay:
hist.age.ppb <- hist(data.la.ppb$Age, main=NULL, xlab="Age", ylab="Number", cex.lab=1.2,breaks=seq(0,max(data.la.ppb$Age),1))

#Save plot to output folder in PNG format
png(paste(out.dir,"/","Age_Freq_Dist_PPB.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
hist(data.la.ppb$Age, main=NULL, xlab="Age", ylab="Number", cex.lab=1.2,breaks=seq(0,max(data.la.ppb$Age),1),col="Light Blue")
dev.off()

#Make a plot of length-frequency distribution of snapper from Port Phillip Bay:
hist.age.ppb <- hist(data.la.ppb$Length, main=NULL, xlab="Length", ylab="Number", cex.lab=1.2, freq=TRUE, breaks=seq(0,max(data.la.ppb$Length)+2,2))

#Save plot to output folder in PNG format
png(paste(out.dir,"/","Length_Freq_Dist_PPB.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
hist.age.ppb <- hist(data.la.ppb$Length, main=NULL, xlab="Length", ylab="Number", cex.lab=1.2, freq=TRUE, breaks=seq(0,max(data.la.ppb$Length)+2,2),col="Pink")
dev.off()

#--------------------------------------
# Set parameters for growth analysis
# -------------------------------------

#Set age of interest (for particular plots);
plot.age <- 4

#Set minimum and maximum cohorts of interest, they are specific to plotting, not to calculations;
cmin <- 1990
cmax <- 2006
cgd.min <- 1992
cgd.max <- 2004

cmin <- NA
cmax <- NA
cgd.min <- NA
cgd.max <- NA

#If cohort min/max values are not specified, then get a best guesss;
if(is.na(cmin)){
	cmin=floor(mean(data.la$Cohort)-2*sd(data.la$Cohort))}
if(is.na(cmax)){
	cmax=ceiling(mean(data.la$Cohort)+2*sd(data.la$Cohort))}
	
if(is.na(cgd.min)){
	cgd.min=floor(mean(data.la$Cohort)-2*sd(data.la$Cohort))}
if(is.na(cgd.max)){
	cgd.max=ceiling(mean(data.la$Cohort)+2*sd(data.la$Cohort))}	

#Use cmin and cmax to get the number of cohorts for plotting and set plot lengths;	
cnum=length(unique(data.la$Cohort)[unique(data.la$Cohort)>=cmin&unique(data.la$Cohort)<=cmax])
cplotx=round(sqrt(cnum))
cploty=ifelse((sqrt(cnum)-floor(sqrt(cnum)))<=0.5,cplotx+1,cplotx)

#Calculate the plotting lengths and widths in terms of years;
ynum=length(unique(data.la$Year)[unique(data.la$Year)])
yplotx=round(sqrt(ynum))
yploty=ifelse((sqrt(ynum)-floor(sqrt(ynum)))<=0.5,yplotx+1,yplotx)

#Create dataframes which split males and female data sets;
data.la.m=data.la[data.la$Sex==1,]
data.la.f=data.la[data.la$Sex==2,]

#Set some objects to help with plotting (maximum age and length from data set);
amax=max(data.la$Age)
lmax=max(data.la$Length)
amax.plot=max(data.la$Age)+2
lmax.plot=max(data.la$Length)+10

#Get maximum and minimum ages for males and females;
amax.m=max(data.la.m$Age)
lmax.m=max(data.la.m$Length)
amax.f=max(data.la.f$Age)
lmax.f=max(data.la.f$Length)


#-------------------------------------
# Analyse growth data:
#-------------------------------------

#Fit a Von Bertalanffy curve to pooled data, then male and female data. Guess starting values suitable for most species; t0=0, Linf=Guess from LMax, k=0.2 
Li=lmax-10
Li.m=lmax.m-10
Li.f=lmax.f-10
K=0.2
t0=0

#Set VonBert Equation;
VonBert=Length~Li*(1-exp(-K*(Age-t0)))

#Fit and predict for VonBert to all data;
vb.model=nls(VonBert,start=list(Li=Li,K=K,t0=t0),data=data.la)
summary(vb.model)
xv=seq(1,amax,0.1)
yv=predict(vb.model,list(Age=xv))

#Fit and predict for VonBert to data from Zone==PPB and Gear==LL:
vb.model.ppb.ll <- nls(VonBert, start=list(Li=Li,K=K,t0=t0), data=data.la.ppb.ll)
summary(vb.model.ppb.ll)
xv <- seq(1,amax,0.1)
yv <- predict(vb.model.ppb.ll,list(Age=xv))

#Fit and predict for VonBert to Males;
vb.model.m=nls(VonBert,start=list(Li=Li.m,K=K,t0=t0),data=data.la.m)
summary(vb.model.m)
xv.m=seq(1,amax.m,0.1)
yv.m=predict(vb.model.m,list(Age=xv.m))

#Fit and predict for VonBert to Females;
vb.model.f=nls(VonBert,start=list(Li=Li.f,K=K,t0=t0),data=data.la.f)
summary(vb.model.f)
xv.f=seq(1,amax.f,0.1)
yv.f=predict(vb.model.f,list(Age=xv.f))

#Get unique points for benefit of plotting;
data.la.plot=unique(data.la[,c("Length","Age")])
data.la.plot.m=unique(data.la.m[,c("Length","Age")])
data.la.plot.f=unique(data.la.f[,c("Length","Age")])

#-------------------------------------
# Plot Growth Data and Model Fits:
#-------------------------------------

# PLOT SET 1: Basic Growth Analysis. Plot observed and expected growth, 
# and compare growth curves between males and females and also with comparison species if required.

#Plot all growth pooled;
plot(data.la.plot$Age,data.la.plot$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",main="Observed & Predicted Length at Age for Snapper")
lines(xv,yv,col="black",lty=1,lwd=3)
  
#Save growth pooled plot to output folder in PNG format
png(paste(out.dir,"/","Observed_and_Predicted_Length_at_Age.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
plot(data.la.plot$Age,data.la.plot$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)")
lines(xv,yv,col="black",lty=1,lwd=3)
dev.off()

#Plot male and female growth separately;
plot(data.la.plot.m$Age,data.la.plot.m$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",main="Observed Length at Age for Snapper",abline(h = 0, col = "grey"))
grid()
points(data.la.plot.f$Age,data.la.plot.f$Length,col="grey")
lines(xv.f,yv.f,col="Red",lty=1,lwd=3)
lines(xv.m,yv.m,col="Dark Blue",lty=1,lwd=3)
legend("topleft", bty = "n", c("Females", "Males"),lty = 1,lwd=2, col = c("Red", "Dark Blue"))

#Save male and female growth plot to output folder in PNG format
png(paste(out.dir,"/","Male_and_Female_Length_at_Age.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
plot(data.la.plot.m$Age,data.la.plot.m$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",abline(h = 0, col = "grey"))
grid()
points(data.la.plot.f$Age,data.la.plot.f$Length,col="grey")
lines(xv.f,yv.f,col="Red",lty=1,lwd=3)
lines(xv.m,yv.m,col="Dark Blue",lty=1,lwd=3)
legend("topleft", bty = "n", c("Females", "Males"),lty = 1,lwd=2, col = c("Red", "Dark Blue"))
dev.off()

#Compare mean length-at-age data with VonBert model on a plot;
plot(data.la.plot$Age,data.la.plot$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",main="Mean vs. VonBert Growth for Snapper")
lines(xv,yv,col="black",lty=1,lwd=2)
MLA=aggregate(data.la$Length,list(Age=data.la$Age),mean)
names(MLA)=c("Age","Mean.Length")
points(MLA,col="Red",pch=18,type="l",lwd=2)

#Save mean length-at-age data with VonBert plot to output folder in PNG format
png(paste(out.dir,"/","Meangrowth_vs_VonBert_model.png",sep=""),width=20,height=20,res=300,units="cm",pointsize=14,bg="white")
plot(data.la.plot$Age,data.la.plot$Length,pch=1,col="grey",xlim=c(0,amax.plot),ylim=c(0,lmax.plot),xlab="Age (yr)",ylab="Fork Length (cm)",)
lines(xv,yv,col="black",lty=1,lwd=2)
MLA=aggregate(data.la$Length,list(Age=data.la$Age),mean)
names(MLA)=c("Age","Mean.Length")
points(MLA,col="Red",pch=18,type="l",lwd=2)
dev.off()

